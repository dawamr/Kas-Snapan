<?php

use Illuminate\Database\Seeder;
use App\Tahun;
use App\User;
use App\Kelas;
use App\Sekolah;
use App\Role;
class KelasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

      // Membuat role admin
      $ownerRole = new Role();
      $ownerRole->name = "owner";
      $ownerRole->display_name = "Dawam Raja";
      $ownerRole->description = "";
      $ownerRole->save();

      // Membuat role Bendahara
      $petugasRole = new Role();
      $petugasRole->name = "petugas";
      $petugasRole->display_name = "Bendahara Kelas";
      $petugasRole->description = "";
      $petugasRole->save();

      // Membuat role Sekertaris
      // $sekertarisRole = new Role();
      // $sekertarisRole->name = "sekertaris";
      // $sekertarisRole->display_name = "Sekertaris Kelas";
      // $sekertarisRole->description = "";
      // $sekertarisRole->save();

      // Membuat role Siswa
      $siswaRole = new Role();
      $siswaRole->name = "siswa";
      $siswaRole->display_name = "Siswa";
      $siswaRole->description = "";
      $siswaRole->save();

      // Membuat role Guru
      $guruRole = new Role();
      $guruRole->name = "guru";
      $guruRole->display_name = "Guru";
      $guruRole->description = "";
      $guruRole->save();

      // Membuat role Pengelola
      // $adminRole = new Role();
      // $adminRole->name = "admin";
      // $adminRole->display_name = "Pengelola Sekolah";
      // $adminRole->description = "";
      // $adminRole->save();
      //
      // $ortuRole = new Role();
      // $ortuRole->name = "Orangtua";
      // $ortuRole->display_name = "Orang Tua";
      // $ortuRole->description = "";
      // $ortuRole->save();

      // Membuat sample admin
      $owner = new User();
      $owner->nama = 'Admin Dawam';
      $owner->email = 'dawam@dev.io';
      $owner->password = bcrypt('rahasia');
      $owner->save();
      $owner->attachRole($ownerRole);

      //Membuat Tahun
      $data = new Tahun();
      $data->tahun1 = 2017 ;
      $data->tahun2 = 2018;
      $data->save();
      $data = new Tahun();
      $data->tahun1 = 2018 ;
      $data->tahun2 = 2019;
      $data->save();
      $data = new Tahun();
      $data->tahun1 = 2019 ;
      $data->tahun2 = 2020;
      $data->save();

      for ($a=1 ;$a <11 ; $a++) {
        $sekolah[$a] = new \App\Sekolah;
        $sekolah[$a]->nama = "SMK Negri " . $a;
        $sekolah[$a]->email = "smkn".$a."@gmail.com";
        $sekolah[$a]->alamat = "Semarang";
        $sekolah[$a]->save();
          for ($x=1; $x < 2; $x++) {
          $kelas[$x] = new Kelas();
          $kelas[$x]->tahun_id = 2;
          $kelas[$x]->sekolah_id = $sekolah[$a]->id;
          $kelas[$x]->nama = "KLS " . $x;
          $kelas[$x]->jurusan = "Jurusan ";
          $kelas[$x]->save();
          $Registrasi[$x] = new \App\Registrasi;
          $Registrasi[$x]->kelas_id = $kelas[$x]->id;
          $Registrasi[$x]->sekolah_id = $sekolah[$a]->id;
          $Registrasi[$x]->tgl = date("Y-m-d");
          $Registrasi[$x]->kas_mingguan = 5000;
          $Registrasi[$x]->status = 'Actived';
          $Registrasi[$x]->save();
          $kas[$x] = new \App\Kas;
          $kas[$x]->kelas_id = $kelas[$x]->id;
          $kas[$x]->kas_bayar = 0;
          $kas[$x]->kas_rutin = 0;
          $kas[$x]->total_kas = 0;
          $kas[$x]->kas_keluar = 0;
          $kas[$x]->sisa_kas = 0;
          $kas[$x]->save();
          for ($i=1 ;$i <2 ; $i++) {
            $data[$i] = new \App\User;
            $data[$i]->kelas_id = $kelas[$x]->id;
            $data[$i]->sekolah_id  = $sekolah[$a]->id;
            $data[$i]->nama = "Siswa " . $i;
            $data[$i]->password = bcrypt("rahasia");
            $data[$i]->save();
            $data[$i]->email = "siswa".$data[$i]->id."@gmail.com";
            $data[$i]->save();
            $memberRole = Role::where('name', 'siswa')->first();
            $data[$i]->attachRole($memberRole);
            $pemasukan[$i] = new \App\Pemasukan;
            $pemasukan[$i]->user_id = $data[$i]->id;
            $pemasukan[$i]->user_bayar = 0;
            $pemasukan[$i]->save();
          }
          $bendahara = new \App\User;
          $bendahara->kelas_id = $kelas[$x]->id;
          $bendahara->sekolah_id  = $sekolah[$a]->id;
          $bendahara->nama = "Bendahara " . $kelas[$x]->nama;
          $bendahara->password = bcrypt("rahasia");;
          $bendahara->save();
          $bendahara->email = "bendahara".$bendahara->id."@gmail.com";
          $bendahara->save();
          $bendaharaRole = Role::where('name', 'petugas')->first();
          $bendahara->attachRole($bendaharaRole);
          $guru = new \App\User;
          $guru->kelas_id = $kelas[$x]->id;
          $guru->sekolah_id  = $sekolah[$a]->id;
          $guru->nama = "Guru " . $kelas[$x]->nama;
          $guru->password = bcrypt("rahasia");;
          $guru->save();
          $guru->email = "guru".$guru->id."@gmail.com";
          $guru->save();
          $guruRole = Role::where('name', 'guru')->first();
          $guru->attachRole($guruRole);
        }
      }
      for ($a=1 ;$a <16 ; $a++) {
        $sekolah[$a] = new \App\Sekolah;
        $sekolah[$a]->nama = "SMA Negri " . $a;
        $sekolah[$a]->email = "sman".$a."@gmail.com";
        $sekolah[$a]->alamat = "Semarang";
        $sekolah[$a]->save();
        for ($x=1; $x < 2; $x++) {
          $kelas[$x] = new Kelas();
          $kelas[$x]->tahun_id = 2;
          $kelas[$x]->sekolah_id = $sekolah[$a]->id;
          $kelas[$x]->nama = "KLS " . $x;
          $kelas[$x]->jurusan = "Jurusan ";
          $kelas[$x]->save();
          $Registrasi[$x] = new \App\Registrasi;
          $Registrasi[$x]->kelas_id = $kelas[$x]->id;
          $Registrasi[$x]->sekolah_id = $sekolah[$a]->id;
          $Registrasi[$x]->tgl = date("Y-m-d");
          $Registrasi[$x]->kas_mingguan = 5000;
          $Registrasi[$x]->status = 'Actived';
          $Registrasi[$x]->save();
          $kas[$x] = new \App\Kas;
          $kas[$x]->kelas_id = $kelas[$x]->id;
          $kas[$x]->kas_bayar = 0;
          $kas[$x]->kas_rutin = 0;
          $kas[$x]->total_kas = 0;
          $kas[$x]->kas_keluar = 0;
          $kas[$x]->sisa_kas = 0;
          $kas[$x]->save();
          for ($i=1 ;$i <2 ; $i++) {
            $data[$i] = new \App\User;
            $data[$i]->kelas_id = $kelas[$x]->id;
            $data[$i]->sekolah_id  = $sekolah[$a]->id;
            $data[$i]->nama = "Siswa " . $i;
            $data[$i]->password = bcrypt("rahasia");;
            $data[$i]->save();
            $data[$i]->email = "siswa".$data[$i]->id."@gmail.com";
            $data[$i]->save();
            $memberRole = Role::where('name', 'siswa')->first();
            $data[$i]->attachRole($memberRole);
            $pemasukan[$i] = new \App\Pemasukan;
            $pemasukan[$i]->user_id = $data[$i]->id;
            $pemasukan[$i]->user_bayar = 0;
            $pemasukan[$i]->save();
          }
          $bendahara = new \App\User;
          $bendahara->kelas_id = $kelas[$x]->id;
          $bendahara->sekolah_id  = $sekolah[$a]->id;
          $bendahara->nama = "Bendahara " . $kelas[$x]->nama;
          $bendahara->password = bcrypt("rahasia");;
          $bendahara->save();
          $bendahara->email = "bendahara".$bendahara->id."@gmail.com";
          $bendahara->save();
          $guru = new \App\User;
          $guru->kelas_id = $kelas[$x]->id;
          $guru->sekolah_id  = $sekolah[$a]->id;
          $guru->nama = "Guru " . $kelas[$x]->nama;
          $guru->password = bcrypt("rahasia");;
          $guru->save();
          $guru->email = "guru".$guru->id."@gmail.com";
          $guru->save();
        }
      }
      // for ($i=1; $i < 12; $i++) {
      //   $sekolah[$i] = new Sekolah();
      //   $sekolah[$i]->nama = "SMK Negeri " . $i;
      //   $sekolah[$i]->alamat "Semarang";
      //   $sekolah[$i]->email = "smk".$i."@sch.id";
      //   $sekolah[$i]->save();
      //   // for ($a=1; $a < 20; $a++) {
      //   //   $kelas[$a] = new Kelas();
      //   //   $kelas[$a]->tahun_id = 2;
      //   //   $kelas[$a]->sekolah_id = $i;
      //   //   $kelas[$a]->nama = "KLS " . $a;
      //   //   $kelas[$a]->jurusan = "Jurusan ".$a;
      //   //   $kelas[$a]->save();
      //   // }
      // }
      // for ($i=1; $i < 12; $i++) {
      //   $sekolah[$i] = new Sekolah();
      //   $sekolah[$i]->nama = "SMA Negeri " . $i;
      //   $sekolah[$i]->alamat "Semarang";
      //   $sekolah[$i]->email = "sma".$i."@sch.id";
      //   $sekolah[$i]->save();
      //   // for ($a=1; $a < 20; $a++) {
      //   //   $kelas[$a] = new Kelas();
      //   //   $kelas[$a]->tahun_id = 2;
      //   //   $kelas[$a]->sekolah_id = $i;
      //   //   $kelas[$a]->nama = "KLS " . $a;
      //   //   $kelas[$a]->jurusan = "Jurusan ".$a;
      //   //   $kelas[$a]->save();
      //   // }
      // }


      // // Membuat Kelas
      // $data->tahun_id = 2;
      // $data = new Kelas();
      // $data->sekolah_id = 2;
      // $data->nama = "XII PS 1";
      // $data->jurusan = "Perawatan Sosial";
      // $data->save();
      // $data = new Kelas();
      // $data->tahun_id = 2;
      // $data->sekolah_id = 2;
      // $data->nama = "XII PS 2";
      // $data->jurusan = "Perawatan Sosial";
      // $data->save();
      // $data = new Kelas();
      // $data->tahun_id = 2;
      // $data->sekolah_id = 2;
      // $data->nama = "XII PS 3";
      // $data->jurusan = "Perawatan Sosial";
      // $data->save();
      // $data = new Kelas();
      // $data->tahun_id = 2;
      // $data->sekolah_id = 2;
      // $data->nama = "XII RPL 1";
      // $data->jurusan = "Rekayasa Perangkat Lunak";
      // $data->save();
      // $data = new Kelas();
      // $data->tahun_id = 2;
      // $data->sekolah_id = 2;
      // $data->nama = "XII RPL 2";
      // $data->jurusan = "Rekayasa Perangkat Lunak";
      // $data->save();
      // $data = new Kelas();
      // $data->tahun_id = 2;
      // $data->sekolah_id = 2;
      // $data->nama = "XII RPL 3";
      // $data->jurusan = "Rekayasa Perangkat Lunak";
      // $data->save();

    }
}
