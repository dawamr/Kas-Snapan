<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKelasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kelas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tahun_id')->unsigned()->nullable();
            $table->foreign('tahun_id')->references('id')->on('tahuns')->onDelete('cascade')->onUpdate('cascade');
            $table->integer('sekolah_id')->unsigned();
            $table->foreign('sekolah_id')->references('id')->on('sekolahs')->onDelete('cascade')->onUpdate('cascade');
            $table->string('nama');
            $table->string('jurusan');
            $table->string('nama_apl')->nullable();
            $table->string('singkatan_apl')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kelas');
    }
}
