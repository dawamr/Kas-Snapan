<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailAbsensisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::create('detail_absensis', function (Blueprint $table) {
        //     $table->increments('id');
        //     $table->integer('absensi_id')->unsigned()->unique();
        //     $table->foreign('absensi_id')->references('id')->on('absensis')->onDelete('cascade')->onUpdate('cascade');
        //     $table->string('status')->nullable();
        //     $table->string('keterangan')->nullable();
        //     $table->timestamps();
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_absensis');
    }
}
