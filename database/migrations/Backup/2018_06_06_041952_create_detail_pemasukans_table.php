<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailPemasukansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_pemasukans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pemasukan_id')->unsigned();
            $table->foreign('pemasukan_id')->references('id')->on('pemasukans')->onDelete('cascade')->onUpdate('cascade');
            $table->integer('bayar');
            $table->date('tgl');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_pemasukans');
    }
}
