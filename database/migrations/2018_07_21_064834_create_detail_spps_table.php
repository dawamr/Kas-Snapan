<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailSppsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::create('detail_spps', function (Blueprint $table) {
        //   $table->increments('id');
        //   $table->integer('spp_id')->unsigned();
        //   $table->foreign('spp_id')->references('id')->on('spps')->onDelete('cascade')->onUpdate('cascade');
        //   $table->integer('bayar');
        //   $table->timestamps();
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_spps');
    }
}
