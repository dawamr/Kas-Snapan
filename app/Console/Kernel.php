<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
      // $schedule->call(function () {
      //   $data = \App\User::select('users.id as id')->orderBy('id', 'asc')->get();
      //     for ($i=1; $i <= count($data); $i++) {
      //       $pemasukan[$i] = \App\Pemasukan::find($i);
      //       $pemasukan[$i]->kas_bayar = $pemasukan[$i]->kas_bayar+10000;
      //       $pemasukan[$i]->save();
      //     }
      // })->everyMinutes();
        // $schedule->command('inspire')
        //          ->hourly();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
