<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PetugasKelasController extends Controller
{
  public function index(){
    $a = \Auth::user();
    $data['kelas'] = \App\Kelas::where('id', \Auth::user()->kelas_id)->first();
    $data['siswa'] = \DB::table('role_user')->where('role_id', '<', 4)
                ->where('role_user.role_id','>',1)
                ->join('users', 'role_user.user_id', '=', 'users.id')
                ->where('users.kelas_id', $a->kelas_id)
                ->join('pemasukans','pemasukans.user_id','=','users.id')
                ->join('kas','users.kelas_id', '=','kas.kelas_id')
                ->select('pemasukans.id as id','users.id as uid','users.nis as nis','users.id as iduser', 'kas.id as idkas','users.nama as nama', 'users.kelas_id as kelas_id', 'kas.kas_bayar as kas_bayar', 'pemasukans.user_bayar as user_bayar')->orderBy('nama', 'asc')
                ->get();
   return view('bendahara.db-kelas.index')->with($data);
  }

  public function detail(Request $request, $id){
    $a = \Auth::user();
    $data['siswa'] = \App\User::join('pemasukans','users.id','=','pemasukans.user_id')
        ->where('users.kelas_id', $a->kelas_id)
        ->where('pemasukans.id', $id)->first();
    $data['detail'] = \App\Pemasukan::join('detail_pemasukans','pemasukans.id','=','detail_pemasukans.pemasukan_id')
        ->where('pemasukans.id',$id)
        ->join('users', 'pemasukans.user_id','=','users.id')
        ->select('detail_pemasukans.*','users.id as user_id', 'users.nama as name', 'pemasukans.user_bayar as user_bayar', 'pemasukans.id as pemasukans_id')
        ->get();

    return view('bendahara.db-kelas.detail')->with($data);
  }

  public function add(Request $request){
    $siswa = \App\Pemasukan::join('users', 'pemasukans.user_id','=','users.id')
            ->where('users.id',$request->user_id)
            ->select('users.*', 'pemasukans.id as pemasukan_id', 'pemasukans.user_bayar as user_bayar')
            ->get()->first();
            // dd($siswa);
    return view('bendahara.db-kelas.add')->with('siswa', $siswa);
  }

  public function store(Request $request){
    $data = \App\Pemasukan::find($request->pemasukan_id);
    $data->user_id = $request->user_id;
    $data->user_bayar = ($request->user_bayar + $request->bayar);
    $data->save();
    $data = new \App\DetailPemasukan;
    $data->pemasukan_id = $request->pemasukan_id;
    $data->bayar = $request->bayar;
    // $data->tanggal = $request->date;
    $data->save();
    $data = \App\Kas::find(\Auth::user()->kelas_id);
    $data->total_kas = $data->total_kas + $request->bayar;
    $data->sisa_kas = $data->total_kas - $data->kas_keluar;
    $data->save();
    if ($data->save  == true) {
        return redirect('petugas/kelas/'.$request->pemasukan_id.'/detail')->with(['success' => 'Data Tersimpan !']);
    }
    if ($data->save  == false) {
      return redirect('petugas/kelas/'.$request->pemasukan_id.'/detail')->with(['wrong' => 'Gagal Menyimpan !']);;
    }
  }
  public function destroy(Request $request, $id){
    $data = \App\DetailPemasukan::find($id);
    $bayar = $data->bayar;
    $datab =\App\Pemasukan::find($request->pemasukan_id);
    $datab->user_bayar = ($datab->user_bayar - $bayar);
    $datac = \App\Kas::find(\Auth::user()->kelas_id);
    $datac->sisa_kas = $datac->sisa_kas - $bayar;
    $datac->save();
    $datab->save();
    $data->delete();
    return redirect()->back();
  }
  public function xdestroy(Request $request,$id){
    $data = \App\Pengeluaran::find($id);
    $udata = \App\Pemasukan::where('user_id',$request->user_id)->first();
    $udata->user_bayar = $udata->user_bayar +$data->uang_keluar;
    $kas = \App\Kas::find(\Auth::user()->kelas_id);
    $kas->kas_keluar = $kas->kas_keluar - $request->uang_keluar;
    $kas->sisa_kas = $kas->sisa_kas + $request->uang_keluar;
    $udata->save();
    $kas->save();
    $data->delete();
    return redirect()->back();
  }

  public function kas_mingguan($id){
        $pemasukan = \App\Kas::find($id);
        $pemasukan->kas_bayar = $pemasukan->kas_bayar+$pemasukan->kas_rutin;
        $pemasukan->save();

      return redirect()->back();
  }
}
