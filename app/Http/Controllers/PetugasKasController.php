<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PetugasKasController extends Controller
{
    public function index(){
      $a = \Auth::user();
      $siswa = \DB::table('role_user')->where('role_id', '<', 4)
                   ->where('role_user.role_id','>',1)
                   ->join('users', 'role_user.user_id', '=', 'users.id')
                   ->join('pemasukans','pemasukans.user_id','=','users.id')
                   ->join('kas','users.kelas_id', '=','kas.kelas_id')
                   ->where('users.kelas_id', $a->kelas_id)
                   ->select('pemasukans.id as id','users.id as iduser', 'kas.id as idkas','users.nama as nama', 'kas.kas_bayar as kas_bayar', 'pemasukans.user_bayar as user_bayar')->orderBy('nama', 'asc')
                   ->orderBy('nama','asc')->get();
      return view('bendahara.db-kas.index')->with('siswa', $siswa);
    }

    public function store(Request $request){
      $totbayar = 0;
      if ($request->bayar2 != null) {
        $data3 = \App\Kas::find(\Auth::user()->kelas_id);
        $data3->total_kas = $data3->total_kas + $request->bayar2;
        $data3->sisa_kas = $data3->total_kas - $data3->kas_keluar;
        $data3->save();
      }
      for ($i=0; $i < count($request->id); $i++) {
        if ($request->bayar[$i] !=null) {
          $data = new \App\DetailPemasukan;
          $data->pemasukan_id = $request->id[$i];
          $data->bayar = $request->bayar[$i];
          // $data->tanggal = $request->date;
          $data->save();
          $data2 = \App\Pemasukan::find($request->id[$i]);
          $data2->user_bayar = ($request->user_bayar[$i] + $request->bayar[$i]);
          $data2->save();
          $totbayar = $totbayar + $request->bayar[$i];
        }
      }
      $data3 = \App\Kas::find(\Auth::user()->kelas_id);
      $data3->total_kas = $data3->total_kas + $totbayar;
      $data3->sisa_kas = $data3->total_kas - $data3->kas_keluar;
      $data3->save();
      if ($data3->save  = true) {
          return redirect()->back()->with(['success' => 'Data Tersimpan !']);
      }
      if ($data->save  = false) {
          return redirect()->back()->with(['wrong' => 'Gagal Menyimpan !']);
      }

    }

    public function keluar(){
      $data['data'] = \DB::table('role_user')->where('role_id', '<', 4)
                   ->where('role_user.role_id','>',1)
                   ->join('users', 'role_user.user_id', '=', 'users.id')
                   ->join('pemasukans','users.id','=','pemasukans.user_id')
                   ->where('kelas_id', \Auth::user()->kelas_id)
                   ->select('users.id as id','users.nama as nama','pemasukans.id as pemasukan_id')
                   ->orderBy('nama','asc')->get();
      return view('bendahara.db-kas.keluar')->with($data);
    }

    public function keluarStore(Request $request){
      // dd($request);
      $testKAS = \App\Kas::where('kelas_id', \Auth::user()->kelas_id)->first();

      if ($testKAS->sisa_kas >= $request->keluar) {
        $data = new \App\Pengeluaran;
        $mdata = \App\Pemasukan::where('user_id', $request->user_id)->first();
        if($request->user_id != null || $request->user_id != ""){
          $data->user_id = $request->user_id;
          $mdata->user_bayar = $mdata->user_bayar - $request->keluar;
          $mdata->save();
        }
        if ($request->user_id == null || $request->user_id == "") {
            $data->user_id = \Auth::user()->id;
        }
        $data->uang_keluar = $request->keluar;
        $data->ket = $request->keterangan;
        $data->save();
        $data= \App\Kas::find(\Auth::user()->kelas_id);
        $data->kas_keluar = $data->kas_keluar + $request->keluar;
        $data->sisa_kas = $data->sisa_kas - $request->keluar;
        $data->save();
        if ($data->save  = true) {
            return redirect()->back()->with(['success' => 'Data Tersimpan !']);
        }
        if ($data->save  = false) {
            return redirect()->back()->with(['wrong' => 'Gagal Menyimpan !']);
        }
      }
      if ($testKAS->sisa_kas <= $request->keluar) {
            return redirect()->back()->with(['wrong' => 'Uang KAS tidak mencukupi !']);
      }

    }

}
