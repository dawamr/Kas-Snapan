<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      if(\Auth::user()->hasRole('owner')){
         return redirect('/owner');
     }
     if(\Auth::user()->hasRole('siswa')){
        return redirect('/siswa');
     }
     if(\Auth::user()->hasRole('petugas')){
         return redirect('/petugas');
     }
     if(\Auth::user()->hasRole('admin')){
         return redirect('/admin');
     }
    }

    public function wellcome(){
      return view('home');
    }

    public function logout(Request $request){
      $request->session()->flush();

      return redirect('/');
    }

}
