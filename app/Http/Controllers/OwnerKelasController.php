<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Kelas;
use \App\user;
class OwnerKelasController extends Controller
{
    public function index(Request $request, $tahun){
      $data['tahun']= \App\Tahun::get();
      $data['kelas'] = Kelas::where('kelas.sekolah_id', $request->idsekolah)
                      ->select('kelas.id as id', 'kelas.nama as nama', 'kelas.jurusan as jurusan')->paginate(7);
      $data['idsekolah'] = $request->idsekolah;
      return view('owner.sekolah.kelas.index')->with($data);
    }

    public function view($id){
      $data['data'] = \App\Kelas::where('sekolah_id', $id)->paginate(3);
      $data['idsekolah'] = $id;
      return view('owner.db-sekolah.db-kelas.index')->with($data);
    }


    public function edit(Request $request, $id){
      // dd($request);
      $data['kelas'] = \App\Kelas::where('id',$id)->first();
      $data['idsekolah'] = $request->sekolah_id;
      return view('owner.db-sekolah.db-kelas.edit')->with($data);
    }
    public function add($idsekolah){
      $idsekolah = $idsekolah;
      return view('owner.db-sekolah.db-kelas.add')->with('idsekolah', $idsekolah);
    }

    public function update(Request $request,$id){
      $data = \App\Kelas::find($id);
      $data->nama = $request->nama;
      $data->jurusan = $request->jurusan;
      $data->tahun_id = $request->tahun_id;
      $data->save();

      return redirect('/owner/db-sekolah/'.$request->sekolah_id.'/view');
    }
    public function store(Request $request){
      $data = new \App\Kelas;
      $data->sekolah_id = $request->sekolah_id;
      $data->nama = $request->nama;
      $data->jurusan = $request->jurusan;
      $data->tahun_id = $request->tahun_id;
      $data->save();

      return redirect('/owner/db-sekolah/'.$request->sekolah_id.'/view');
    }

    public function destroy($id){
      $data = \App\Kelas::find($id);
      $data->delete();
      return redirect()->back();
    }
}
