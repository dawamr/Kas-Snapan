<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class OwnerSekolahController extends Controller
{
  
    public function index(){
      $sekolah = \App\Sekolah::select('sekolahs.id as id', 'sekolahs.nama as nama','sekolahs.alamat as alamat', 'sekolahs.email as email')->orderBy('nama', 'asc')->get();
      // dd($sekolah);
      return view('owner.sekolah.index',compact('sekolah'));
    }
    public function add(){
      return view('owner.sekolah.add');
    }
    public function edit($id){
      $sekolah = \App\Sekolah::find($id);
      return view('owner.sekolah.edit')->with('sekolah', $sekolah);
    }
    public function store(Request $request){
      $data = new \App\Sekolah;
      $data->nama = $request->nama;
      $data->alamat = $request->alamat;
      $data->email = $request->email;
      $data->save();
      return redirect('/owner/sekolah');
    }
    public function update(Request $request, $id){
      $data = \App\Sekolah::find($id);
      $data->nama = $request->nama;
      $data->alamat = $request->alamat;
      $data->email = $request->email;
      $data->save();
      return redirect('/owner/sekolah');
    }
}
