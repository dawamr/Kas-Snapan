<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PetugasLaporanController extends Controller
{
    public function dashboard(){
      $data['data'] = \App\Kas::where('kelas_id',\Auth::user()->kelas_id)->first();
      $data['sisa'] = 0;
      $data['keluar'] = 0;
      $data['total'] = 0;
      if($data['data']->total_kas>0){
        $data['total'] = 100;
        $data['sisa'] = ($data['data']->sisa_kas/$data['data']->total_kas)*100;
        $data['keluar'] = ($data['data']->kas_keluar/$data['data']->total_kas)*100;
      }
      $data['best1'] = \App\Kas::orderBy('total_kas', 'decs')->join('kelas', 'kas.kelas_id','=','kelas.id')
                       ->select('kelas.nama as nama', 'kas.total_kas as total_kas')->first();
     $data['best2'] = \App\Kas::orderBy('kas_keluar', 'decs')->join('kelas', 'kas.kelas_id','=','kelas.id')
                      ->select('kelas.nama as nama', 'kas.kas_keluar as kas_keluar')->first();
     $data['best3'] = \App\Pemasukan::orderBy('user_bayar','decs')->join('users','pemasukans.user_id','=','users.id')
                      ->select('users.nama as nama', 'pemasukans.user_bayar as user_bayar')->first();
      return view('bendahara.dashboard')->with($data);
    }

    public function index(){
      $data['bulan'] = ['Januari','Febuari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];
      return view('bendahara.db-laporan.index')->with($data);
    }

    public function detail(Request $request){
      $data['bulan'] = $request->id;
      $data['siswa'] = \DB::table('role_user')->where('role_id', '<', 4)
                  ->where('role_user.role_id','>',1)
                  ->join('users', 'role_user.user_id', '=', 'users.id')
                  ->where('users.kelas_id', \Auth::user()->kelas_id)
                  ->select('users.id as id','users.nama as nama')
                  ->orderBy('nama', 'asc')
                  ->get();
      return view('bendahara.db-laporan.detail')->with($data);
    }
}
