<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class PetugasSettingController extends Controller
{
    public function index(){
      $data = \Auth::user()->join('kas','users.kelas_id','kas.kelas_id')->first();
      return view('bendahara.setting')->with('data', $data);
    }

    public function kas_rutin(Request $request){
      $data = \App\Kas::find(\Auth::User()->kelas_id);
      $data->kas_rutin = $request->kas_rutin;
      $data->save();
      if ($data->save  = true) {
          return redirect()->back()->with(['success1' => 'Pengaturan Tersimpan !']);
      }
      if ($data->save  = false) {
          return redirect()->back()->with(['wrong1' => 'Gagal Menyimpan !']);
      }
    }
    public function setApp(Request $request){
      $app = \App\Kelas::find(\Auth::User()->kelas_id);
      $app->nama_apl = $request->AppName;
      $app->singkatan_apl = $request->AppName1;
      $app->save();
      if ($app->save  = true) {
          return redirect()->back()->with(['success2' => 'Pengaturan Tersimpan !']);
      }
      if ($app->save  = false) {
          return redirect()->back()->with(['wrong2' => 'Gagal Menyimpan !']);
      }
    }
}
