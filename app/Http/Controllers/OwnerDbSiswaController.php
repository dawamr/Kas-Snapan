<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
class OwnerDbSiswaController extends Controller
{
  public function sekolah(){
    $sekolah = \App\Sekolah::get();
    return view('owner.db-siswa.sekolah'
    )->with('sekolah', $sekolah);
  }

  public function index($id){
    $siswa = \App\User::join('kelas', 'users.kelas_id','=','kelas.id')->orderBy('kelas.nama','asc')
    ->join('role_user','user_id','=','users.id')->where('role_user.role_id','<',4)
    ->where('role_user.role_id','>',1)->where('kelas.sekolah_id',$id)
    ->select('users.*','kelas.nama as nama_kelas','kelas.sekolah_id as sekolah_id')
    ->paginate(18);
    return view('owner.db-siswa.index',compact('id','siswa'));
  }
  public function add($id){
    return view('owner.db-siswa.add')->with('id',$id);
  }
  public function edit(Request $request, $id){
    $siswa = \App\User::find($id);
    return view('owner.db-siswa.edit')->with('siswa', $siswa);
  }
  public function store(Request $request, $id){
    $data = new \App\User;
    $data->nis = $request->nis;
    $data->nama = $request->nama;
    $data->email = $request->email;
    $data->password = bcrypt($request->password);
    $data->kelas_id = $request->kelas_id;
    $data->save();
    $memberRole = \App\Role::where('id', $request->role_id)->first();
    $data->attachRole($memberRole);
    $pemasukan = new \App\Pemasukan;
    $pemasukan->user_id = $data['id'];
    $pemasukan->user_bayar = 0;
    $pemasukan->save();

    return redirect('/owner/db-siswa/'.$id.'');
  }
  public function update(Request $request, $id){
    $data = \App\User::find($id);
    $data->nis = $request->nis;
    $data->nama = $request->nama;
    $data->email = $request->email;
    if($request->password!=null){
      $data->password = bcrypt($request->password);
    }
    $data->save();
    $sekolah_id = $request->sekolah_id;
    return redirect('/owner/db-siswa/'.$sekolah_id.'');
  }
  public function destroy($id){
    $data = \App\User::find($id);
    $data->delete();

    return redirect()->back();
  }
}
