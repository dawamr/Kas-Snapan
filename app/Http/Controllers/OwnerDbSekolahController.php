<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class OwnerDbSekolahController extends Controller
{
    public function index(){
      $sekolah = \App\Sekolah::orderBy('nama','asc')->get();
      return view('owner.db-sekolah.index')->with('sekolah', $sekolah);
    }
    public function add(){
      return view('owner.db-sekolah.add');
    }
    public function edit($id){
      $sekolah = \App\Sekolah::find($id);
      return view('owner.db-sekolah.edit')->with('sekolah', $sekolah);
    }
    public function store(Request $request){
      $sekolah = new \App\Sekolah;
      $sekolah->nama = $request->nama;
      $sekolah->alamat = $request->alamat;
      $sekolah->email = $request->email;
      $sekolah->save();
      return redirect('/owner/db-sekolah');
    }
    public function update(Request $request, $id){
      $sekolah = \App\Sekolah::find($id);
      $sekolah->nama = $request->nama;
      $sekolah->alamat = $request->alamat;
      $sekolah->email = $request->email;
      $sekolah->save();
      return redirect('/owner/db-sekolah');
    }
    public function destroy($id){
      $data = \App\Sekolah::find($id);
      $data->delete();

      return redirect()->back();
    }
}
