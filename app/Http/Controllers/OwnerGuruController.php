<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class OwnerGuruController extends Controller
{
  public function sekolah(){
    $sekolah = \App\Sekolah::get();
    return view('owner.db-guru.sekolah')->with('sekolah', $sekolah);
  }

  public function index($id){
    $guru = \App\User::join('kelas', 'users.kelas_id','=','kelas.id')->orderBy('kelas.nama','asc')
    ->join('role_user','user_id','=','users.id')->where('role_user.role_id','=',4)
    ->where('kelas.sekolah_id',$id)
    ->select('users.*','kelas.nama as nama_kelas')
    ->paginate(8);
    return view('owner.db-guru.index',compact('id','guru'));
  }
  public function add($id){
    return view('owner.db-guru.add')->with('id',$id);
  }
  public function edit($id){
    $guru = \App\User::find($id);
    return view('owner.db-guru.edit')->with('guru', $guru);
  }
  public function store(Request $request, $id){
    $data = new \App\User;
    $data->nis = null;
    $data->nama = $request->nama;
    $data->email = $request->email;
    $data->password = bcrypt($request->password);
    $data->kelas_id = $request->kelas_id;
    $data->save();
    $memberRole = \App\Role::where('id', $request->role_id)->first();
    $data->attachRole($memberRole);
    $pemasukan = new \App\Pemasukan;
    $pemasukan->user_id = $data['id'];
    $pemasukan->user_bayar = 0;
    $pemasukan->save();

    return redirect('/owner/db-guru/'.$id.'');
  }
  public function update(Request $request, $id){
    $data = \App\User::find($id);
    $data->nis = null;
    $data->nama = $request->nama;
    $data->email = $request->email;
    if($request->password!=null){
      $data->password = bcrypt($request->password);
    }
    $data->save();
    return redirect()->back();
  }
  public function destroy($id){
    $data = \App\User::find($id);
    $data->delete();

    return redirect()->back();
  }
}
