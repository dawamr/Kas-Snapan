<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\User;
class OwnerSiswaController extends Controller
{
   public function index($id){
     $data['kelas'] = \App\Kelas::where('id', $id)->get()->first();
     $data['siswa'] = \DB::table('role_user')->where('role_id', '<', 4)
                    ->where('role_user.role_id','>',1)
                   ->join('users', 'role_user.user_id', '=', 'users.id')
                   ->join('pemasukans','pemasukans.user_id','=','users.id')
                   ->join('kas','users.kelas_id', '=','kas.kelas_id')
                   ->where('users.kelas_id', $id)
                   ->select('pemasukans.id as id','users.nis as nis','users.id as iduser', 'kas.id as idkas','users.nama as nama', 'kas.kas_bayar as kas_bayar', 'pemasukans.user_bayar as user_bayar')->orderBy('nama', 'asc')
                   ->get();
    return view('owner.sekolah.kelas.siswa.index')->with($data);
   }
   public function kas_mingguan($id){
         $pemasukan = \App\Kas::find($id);
         $pemasukan->kas_bayar = $pemasukan->kas_bayar+$pemasukan->kas_rutin;
         $pemasukan->save();

       return redirect()->back();
   }

}
