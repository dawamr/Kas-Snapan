<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SiswaController extends Controller
{
    public function dashboard(){
      $data['data'] = \App\Kas::where('kelas_id',\Auth::user()->kelas_id)->first();
      $data['sisa'] = 0;
      $data['keluar'] = 0;
      $data['total'] = 0;
      if($data['data']->total_kas>0){
        $data['total'] = 100;
        $data['sisa'] = ($data['data']->sisa_kas/$data['data']->total_kas)*100;
        $data['keluar'] = ($data['data']->kas_keluar/$data['data']->total_kas)*100;
      }
      $data['best1'] = \App\Kas::orderBy('total_kas', 'decs')->join('kelas', 'kas.kelas_id','=','kelas.id')
                       ->select('kelas.nama as nama', 'kas.total_kas as total_kas')->first();
     $data['best2'] = \App\Kas::orderBy('kas_keluar', 'decs')->join('kelas', 'kas.kelas_id','=','kelas.id')
                      ->select('kelas.nama as nama', 'kas.kas_keluar as kas_keluar')->first();
     $data['best3'] = \App\Pemasukan::orderBy('user_bayar','decs')->join('users','pemasukans.user_id','=','users.id')
                      ->select('users.nama as nama', 'pemasukans.user_bayar as user_bayar')->first();
      return view('siswa.dashboard')->with($data);
    }
    public function setting(){
      return view ('siswa.setting');
    }
    public function setAccount(Request $request){
      $data = \App\User::find(\Auth::user()->id);
      $data->nama = $request->nama;
      $data->nis = $request->nis;
      $data->email = $request->email;
      if ($request->password != null || $request->password != "" And $request->re_password != null || $request->re_password != ""){
        if ($request->password == $request->re_password) {
          $data->password = bcrypt($request->password);
        } else {
          return redirect()->back()->with(['wrong' => 'Password Tidak Cocok!']);
        }
      }
      $data->save();
      if ($data->save  = true) {
          return redirect()->back()->with(['success' => 'Data Tersimpan !']);
      }
      if ($data->save  = false) {
          return redirect()->back()->with(['wrong' => 'Gagal Menyimpan !']);
      }
    }
}
