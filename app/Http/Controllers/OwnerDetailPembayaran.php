<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class OwnerDetailPembayaran extends Controller
{
    public function index($id){
      $data['siswa'] = \App\User::join('pemasukans','users.id','=','pemasukans.user_id')
          ->where('pemasukans.id', $id)->get()->first();
      $data['detail'] = \App\Pemasukan::join('detail_pemasukans','pemasukans.id','=','detail_pemasukans.pemasukan_id')
          ->where('pemasukans.id', $id)
          ->join('users', 'pemasukans.user_id','=','users.id')
          ->select('detail_pemasukans.*','users.id as user_id', 'users.nama as name', 'pemasukans.user_bayar as user_bayar', 'pemasukans.id as pemasukans_id')
          ->get();

      return view('owner.sekolah.kelas.siswa.pembayaran.index')->with($data);
    }

    public function add($id){
      $siswa = \App\Pemasukan::join('users', 'pemasukans.user_id','=','users.id')
              ->where('users.id',$id)
              ->select('users.*', 'pemasukans.id as pemasukan_id', 'pemasukans.user_bayar as user_bayar')
              ->get()->first();
              // dd($siswa);
      return view('owner.sekolah.kelas.siswa.pembayaran.add')->with('siswa', $siswa);
    }
    public function store(Request $request, $pemasukan_id){
      $data = \App\Pemasukan::find($pemasukan_id);
      $data->user_id = $request->user_id;
      $data->user_bayar = ($request->user_bayar + $request->bayar);
      $data->save();
      $data = new \App\DetailPemasukan;
      $data->pemasukan_id = $request->pemasukan_id;
      $data->bayar = $request->bayar;
      $data->save();
      $data = \App\Kas::find($request->kelas_id);
      $data->total_kas = $data->total_kas + $request->bayar;
      $data->sisa_kas = $data->total_kas - $data->kas_keluar;
      $data->save();

      return redirect('/owner/siswa/'. $request->pemasukan_id .'');
    }
    public function destroy(Request $request, $id){
      $data = \App\DetailPemasukan::find($id);
      $bayar = $data->bayar;
      $datab =\App\Pemasukan::find($request->pemasukan_id);
      $datab->user_bayar = ($datab->user_bayar - $bayar);
      $datac = \App\Kas::find($request->kelas_id);
      $datac->sisa_kas = $datac->sisa_kas - $bayar;
      $datac->save();
      $datab->save();
      $data->delete();

      return redirect()->back();
    }
}
