<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class OwnerController extends Controller
{
    public function setAccount(Request $request){
      $data = \App\User::find(\Auth::user()->id);
      if($request->nama != null){
        $data->nama = $request->nama;
      }
      if($request->email != null){
        $data->email = $request->email;
      }
      if($request->password != null && $request->rePassword != null){
        if($request->password === $request->rePassword){
          $data->password = bcrypt($request->password);
        } else{
            return redirect('/owner/setting')->with(['wrong1' => 'Password Tidak Sama !']);
        }
      }
      if($data->save()){
        return redirect('/owner/setting')->with(['success1' => 'Berhasil Mengubah Profile !']);
      }
    }

    public function dashboard(){
      return view('owner.dashboard');
    }

    public function setting(){
      return view('owner.setting');
    }

    public function setApp(Request $request){
      try {
        $count = count(\App\Kelas::get());
        for ($i=1; $i <= $count ; $i++) {
          $app[$i] = \App\Kelas::find($i);
          if($request->AppName != null){
            $app[$i]->nama_apl = $request->AppName;
          }
          if($request->AppName1!= null){
            $app[$i]->singkatan_apl = $request->AppName1;
          }
          $app[$i]->save();
        }
      } catch (\Exception $e) {
        return redirect()->back()->with(['wrong2' => 'Gagal Menyimpan !']);
      }finally{
        return redirect()->back()->with(['success2' => 'Pengaturan Tersimpan !']);
      }

      // if($app->save()) {
      //     return redirect()->back()->with(['success2' => 'Pengaturan Tersimpan !']);
      // }else{
      //     return redirect()->back()->with(['wrong2' => 'Gagal Menyimpan !']);
      // }
    }
}
