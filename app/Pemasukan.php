<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pemasukan extends Model
{
  protected $fillable = [
      'user_id', 'user_bayar','kas_bayar',
  ];
}
