<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/home','HomeController@wellcome');
Route::get('/logout', 'HomeController@logout');
Route::get('/test', function(){
  return view('welcome');
});

Auth::routes();
Route::get('/', 'HomeController@index')->name('home');

Route::group(['prefix'=>'owner', 'middleware'=>['auth','role:owner']], function () {

    // Route diisi disini...
    Route::get('/','OwnerController@dashboard');
    Route::get('/setting','OwnerController@setting');
    Route::post('/set-account','OwnerController@setAccount');
    Route::post('/set-app','OwnerController@setApp');
    //ROUTE KELAS
    Route::post('/angkatan/{tahun}', 'OwnerKelasController@index');
    Route::get('/db-sekolah/{id}/view','OwnerKelasController@view');

    Route::post('/db-kelas/{id}/edit','OwnerKelasController@edit');
    Route::get('/{idsekolah}/db-kelas/add','OwnerKelasController@add');
    Route::post('/db-kelas/store','OwnerKelasController@store');
    Route::post('/db-kelas/{id}/update','OwnerKelasController@update');
    Route::post('/db-kelas/{id}/destroy','OwnerKelasController@destroy');

    Route::get('/kelas/{id}', 'OwnerSiswaController@index');
    Route::post('/kelas/{id}/kas_mingguan', 'OwnerSiswaController@kas_mingguan');

    Route::get('/siswa/{iduser}', 'OwnerDetailPembayaran@index');
    Route::get('/siswa/{id}/add', 'OwnerDetailPembayaran@add');
    Route::post('/siswa/{pemasukan_id}', 'OwnerDetailPembayaran@store');
    Route::post('/siswa/{pemasukan_id}/destroy', 'OwnerDetailPembayaran@destroy');

    Route::get('/sekolah', 'OwnerSekolahController@index');
    Route::get('/sekolah/add', 'OwnerSekolahController@add');
    Route::get('/sekolah/{id}/edit', 'OwnerSekolahController@edit');
    Route::post('/sekolah', 'OwnerSekolahController@store');
    Route::post('/sekolah/{id}', 'OwnerSekolahController@update');

    Route::get('/db-sekolah', 'OwnerDbSekolahController@index');
    Route::get('/db-sekolah/add', 'OwnerDbSekolahController@add');
    Route::get('/db-sekolah/{id}/edit','OwnerDbSekolahController@edit');
    Route::post('/db-sekolah','OwnerDbSekolahController@store');
    Route::post('/db-sekolah/{id}/update','OwnerDbSekolahController@update');
    Route::post('/db-sekolah/{id}','OwnerDbSekolahController@destroy');

    Route::get('/{{idsekolah}}/db-kelas/add', 'OwnerDbSekolahController@index');

    Route::get('/db-siswa', 'OwnerDbSiswaController@sekolah');
    Route::get('/db-siswa/{id}', 'OwnerDbSiswaController@index');
    Route::get('/db-siswa/{id}/add', 'OwnerDbSiswaController@add');
    Route::get('/db-siswa/{id}/edit','OwnerDbSiswaController@edit');
    Route::post('/db-siswa/new/{id}','OwnerDbSiswaController@store');
    Route::post('/db-siswa/{id}/update','OwnerDbSiswaController@update');
    Route::post('/db-siswa/{id}','OwnerDbSiswaController@destroy');

    Route::get('/db-guru', 'OwnerGuruController@sekolah');
    Route::get('/db-guru/{id}', 'OwnerGuruController@index');
    Route::get('/db-guru/{id}/add', 'OwnerGuruController@add');
    Route::get('/db-guru/{id}/edit','OwnerGuruController@edit');
    Route::post('/db-guru/new/{id}','OwnerGuruController@store');
    Route::post('/db-guru/{id}/update','OwnerGuruController@update');
    Route::post('/db-guru/{id}','OwnerGuruController@destroy');

});

Route::group(['prefix'=>'siswa', 'middleware'=>['auth','role:siswa']], function () {
    Route::get('/', 'SiswaController@dashboard');
    Route::get('/setting', 'SiswaController@setting');
    Route::post('/setting/account', 'SiswaController@setAccount');

});

Route::group(['prefix'=>'petugas', 'middleware'=>['auth','role:petugas']], function () {
    // Route diisi disini...
    Route::get('/', 'PetugasLaporanController@dashboard');
    Route::get('/laporan', 'PetugasLaporanController@index');
    Route::post('/laporan/detail','PetugasLaporanController@detail');

    Route::get('/kas', 'PetugasKasController@index');
    Route::post('/kas', 'PetugasKasController@store');
    Route::get('/kas/keluar', 'PetugasKasController@keluar');
    Route::post('/kas/keluar', 'PetugasKasController@keluarStore');


    Route::get('/kelas', 'PetugasKelasController@index');
    Route::get('/kelas/{id}/detail', 'PetugasKelasController@detail');
    Route::post('/kelas/bayar-kas', 'PetugasKelasController@add');
    Route::post('/kelas/save', 'PetugasKelasController@store');
    Route::post('/kelas/{id}/destroy', 'PetugasKelasController@destroy');
    Route::post('/kelas/x/{id}/destroy', 'PetugasKelasController@xdestroy');
    Route::post('/kelas/{id}/kas_mingguan', 'PetugasKelasController@kas_mingguan');

    Route::get('/setting', 'PetugasSettingController@index');
    Route::post('/setting/kas_rutin', 'PetugasSettingController@kas_rutin');
    Route::post('/setting/app-setting', 'PetugasSettingController@setApp');
});

//  Route::group(['prefix'=>'admin', 'middleware'=>['auth','role:admin']], function () {
   // Route::get('/', 'PengelolaKelasController@dashboard');
   // Route::get('/kelas', 'PengelolaKelasController@index');
// });
