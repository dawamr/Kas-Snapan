<?php
  $check = \App\Registrasi::where('kelas_id', \Auth::user()->kelas_id)->first();
    $authnya= \DB::table('role_user')->where('role_id', '>', 1)
                ->join('users', 'role_user.user_id', '=', 'users.id')
                ->where('id', \Auth::user()->id)->first();
 ?>
 @if ($authnya == true)
   @if($check->status != 'Actived')
     <script type="text/javascript">
       window.location.href = "/home"
     </script>
   @endif
 @endif

  <!DOCTYPE html>
  <html>

  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>KAS ONLINE SYSTEM</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{URL::asset('adminlte/bootstrap/dist/css/bootstrap.min.css')}} ">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{URL::asset('adminlte/font-awesome/css/font-awesome.min.css')}} ">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{URL::asset('adminlte/Ionicons/css/ionicons.min.css')}} ">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{URL::asset('adminlte/dist/css/AdminLTE.min.css')}}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{URL::asset('adminlte/dist/css/skins/_all-skins.min.css')}}">
    <!-- Morris chart -->
    <link rel="stylesheet" href="{{URL::asset('adminlte/morris.js/morris.css')}} ">
    <!-- jvectormap -->
    <link rel="stylesheet" href="{{URL::asset('adminlte/jvectormap/jquery-jvectormap.css')}} ">
    <!-- Date Picker -->
    <link rel="stylesheet" href="{{URL::asset('adminlte/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}} ">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="{{URL::asset('adminlte/bootstrap-daterangepicker/daterangepicker.css')}} ">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

    <link rel="stylesheet" type="text/css" href="/css/sweetalert.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    @yield('css')

  </head>

  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

      <header class="main-header">
        <!-- Logo -->
        <a href="#" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <?php
        $app = \App\Kelas::where('id',\Auth::user()->kelas_id)->first();
       ?>

      @if ($authnya == true)
         @if ($app->singkatan_apl != null)
           <span class="logo-mini"><b>{{$app->singkatan_apl}}</b></span>
         @endif
         @if ($app->singkatan_apl == null)
           <span class="logo-mini"><b>KA</b>LINE</span>
         @endif
         @if ($app->nama_apl != null)
           <span class="logo-lg"><b>{{$app->nama_apl}}</b></span>
         @endif
         @if ($app->nama_apl == null)
           <span class="logo-lg"><b>KAS</b>Online</span>
         @endif
       @endif
       @if ($authnya == false)
         <span class="logo-mini"><b>KA</b>LINE</span>
         <span class="logo-lg"><b>KAS</b>Online</span>
       @endif
      <!-- logo for regular state and mobile devices -->
    </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- Messages: style can be found in dropdown.less-->

              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="{{URL::asset('adminlte/dist/img/user2-160x160.jpg')}}" class="user-image" alt="User Image">
              <span class="hidden-xs">
                <?php
                $user = Auth::user();
              ?>
            </span>
            </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <img src="{{URL::asset('adminlte/dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">

                    <p>
                      {{$user->nama}} - @if($user->hasRole('owner')) Owner
                        @endif
                        @if($user->hasRole('petugas')) Bendahara
                          @endif
                          <small>Kas Online {{date('Y')}}</small>
                    </p>
                  </li>
                  <!-- Menu Body -->
                  <li class="user-body">
                    <div class="row">
                      <div class="col-xs-12 text-center">
                        <a href="#">{{$user->nama}}</a>
                      </div>
                    </div>
                    <!-- /.row -->
                  </li>
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="#" class="btn btn-default btn-flat">Profile</a>
                    </div>
                    <div class="pull-right">
                      <a href="/logout" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
              <!-- Control Sidebar Toggle Button -->
              <!-- <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li> -->
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="{{URL::asset('adminlte/dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
        </div>

              <div class="pull-left info">
                <p>{{$user->nama}}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
              </div>
            </div>
            <!-- search form -->
            <!-- <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form> -->
            <!-- /.search form -->
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu" data-widget="tree">
              <li class="header">MAIN NAVIGATION</li>

              @if(Auth::user()->hasRole('owner'))
                <li><a href="/owner"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
                <li><a href="/owner/sekolah"><i class="fa fa-user"></i> <span>KAS Management</span></a></li>
                <li><a href="/owner/db-sekolah"><i class="fa fa-university"></i> <span>Sekolah</span></a></li>
                <li><a href="/owner/db-siswa"><i class="fa fa-group"></i> <span>Siswa</span></a></li>
                <li><a href="/owner/db-guru"><i class="fa fa-group"></i> <span>Guru</span></a></li>
                <li><a href="/owner/setting"><i class="fa fa-gear"></i> <span>Pengaturan</span></a></li>
              @endif
                @if(Auth::user()->hasRole('petugas'))
                  <li><a href="/petugas"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
                  <li><a href="/petugas/kas"><i class="fa fa-user"></i> <span>KAS Management</span></a></li>
                  <li><a href="/petugas/kelas"><i class="fa fa-university"></i> <span>Kelas</span></a></li>
                  <li><a href="/petugas/laporan"><i class="fa fa-book"></i> <span>Laporan</span></a></li>
                  <li><a href="/petugas/setting"><i class="fa fa-gear"></i> <span>Pengaturan</span></a></li>
                @endif
                  @if(Auth::user()->hasRole('siswa'))
                    <li><a href="/siswa"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
                    <li><a href="/siswa/setting"><i class="fa fa-gear"></i> <span>Pengaturan</span></a></li>
                  @endif
            </ul>
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
        @yield('header')
        <small>Control panel</small>
      </h1>
          <ol class="breadcrumb">
            <li><a href="/petugas"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol>
          <br>
    </section>

          <!-- Main content -->
          <section class="content">

            @yield('content')
          </section>
          <!-- /.content -->
      </div>
      <!-- /.content-wrapper -->
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 2.4.0
        </div>
        <strong>Copyright &copy; 2018-2019 <a href="https://adminlte.io">Dawam Raja</a>.</strong> All rights
        reserved.
      </footer>

    </div>
    <!-- ./wrapper -->
    @yield('js')
    <!-- jQuery 3 -->
    <script src="{{URL::asset('adminlte/jquery/dist/jquery.min.js')}}"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="{{URL::asset('adminlte/jquery-ui/jquery-ui.min.js')}}"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
      $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.7 -->
    <script src="{{URL::asset('adminlte/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <!-- VueJS -->
    <!-- <script src="{{URL::asset('js/vue.js')}}"></script>
<script src="{{URL::asset('js/axios.js')}}"></script> -->
    <!-- Morris.js charts -->
    <script src="{{URL::asset('adminlte/raphael/raphael.min.js')}}"></script>
    <script src="{{URL::asset('adminlte/morris.js/morris.min.js')}}"></script>
    <!-- Sparkline -->
    <script src="{{URL::asset('adminlte/jquery-sparkline/dist/jquery.sparkline.min.js')}}"></script>
    <!-- jvectormap -->
    <script src="{{URL::asset('adminlte/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
    <script src="{{URL::asset('adminlte/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
    <!-- jQuery Knob Chart -->
    <script src="{{URL::asset('adminlte/jquery-knob/dist/jquery.knob.min.js')}}"></script>
    <!-- daterangepicker -->
    <script src="{{URL::asset('adminlte/moment/min/moment.min.js')}}"></script>
    <script src="{{URL::asset('adminlte/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
    <!-- datepicker -->
    <script src="{{URL::asset('adminlte/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="{{URL::asset('adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
    <!-- Slimscroll -->
    <script src="{{URL::asset('adminlte/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    <!-- FastClick -->
    <script src="{{URL::asset('adminlte/fastclick/lib/fastclick.js')}}"></script>
    <!-- AdminLTE App -->
    <script src="{{URL::asset('adminlte/dist/js/adminlte.min.js')}}"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="{{URL::asset('adminlte/dist/js/pages/dashboard.js')}}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{URL::asset('adminlte/dist/js/demo.js')}}"></script>
  </body>

  </html>
