
@extends('layouts.lte')
@section('css')
  <style>

    .container{
      background: white;
      border-radius: 4px;
    }
    .break{
      background: #e4e3e3;
    }
  </style>
@endsection
@section('header')
Pengaturan KAS Kelas
@endsection
@section('content')

  <div class="container p-4">
    <h1 class="text-center">Pengaturan Akun</h1>
    <hr>
    <div class="col-md-12">
      <div class="col-md-6">
        <div class="card">
          <h3 class="text-center">Detail Akun</h3>
          <hr>
          <br>
          @php
            $a = \Auth::user();
          @endphp
          <div class="row">
            <div class="col-md-12">
              @if($message = Session::get('success'))
              <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert" name="button">x</button>
                <strong>{{$message}}</strong>
              </div>
            <hr>
              @endif
              @if($message = Session::get('wrong'))
              <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert" name="button">x</button>
                <strong>{{$message}}</strong>
              </div>
            <hr>
              @endif
              
              <form class="form" action="/siswa/setting/account" method="post">
              {{csrf_field()}}
                <table class="table">
                  <tr>
                    <td width="30%" style="font-weight: bold">Nama</td>
                    <td width="5%" > : </td>
                    <td width="65%" >
                    <input class="form-control" type="text" name="nama" value="{{$a->nama}}">
                    </td>
                  </tr>
                  <tr>
                    <td width="30%" style="font-weight: bold">NIS</td>
                    <td width="5%" > : </td>
                    <td width="65%" >
                    <input class="form-control" type="text" name="nis" value="{{$a->nis}}">
                    </td>
                  </tr>
                  <tr>
                    <td width="30%" style="font-weight: bold">Email</td>
                    <td width="5%" > : </td>
                    <td width="65%" >
                    <input class="form-control" type="text" name="email" value="{{$a->email}}">
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <a style="cursor:pointer;" onclick="openPass()">Ganti Password</a>
                    </td>
                  </tr>
                  <tr>
                    <td width="30%" style="font-weight: bold">New Password</td>
                    <td width="5%" > : </td>
                    <td width="65%" >
                    <input class="form-control" id="myPass1" type="password" name="password" value="" disabled>
                    </td>
                  </tr>
                  <tr>
                    <td width="30%" style="font-weight: bold">Retyping Password</td>
                    <td width="5%" > : </td>
                    <td width="65%" >
                    <input class="form-control" id="myPass2" type="password" name="re_password"  value="" disabled>
                    </td>
                  </tr>
                  </table>
                  <div class="btn-group" role="group" aria-label="basic example">
                    <button type="submit" onclick="required()" class="btn btn-secondary btn-primary" name="button"><i class="fa fa-pencil"></i> &nbsp Simpan</button>
                    <button type="button" class="btn btn-secondary btn-danger" name="button" onclick="batal()"><i class="fa fa-close"></i> &nbsp Batal</button>
                    <br>
                  </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
@section('js')
<script type="text/javascript">
  function openPass(){
    document.getElementById('myPass1').disabled = false;
    document.getElementById('myPass2').disabled = false;
  }
  function batal(){
    document.getElementById('myPass1').disabled = true;
    document.getElementById('myPass2').disabled = true;
    document.getElementById('myPass1').value = "";
    document.getElementById('myPass2').value = "";
  }

</script>
@endsection
