
@extends('layouts.lte')
@section('css')
  <style>

    .container{
      background: white;
      border-radius: 4px;
    }
    .space{
      background-color: #e4e3e3;
    }
  </style>
@endsection
@section('header')
Pengaturan KAS Kelas
@endsection
@section('content')
<div class="container p-4" >
  <h1 class="text-center">Rincian KAS Kelas</h1>
  <hr>
  <div class="col-12">
    <h2 class="text-center">Presentase KAS</h2>
    <hr>

    <div class="row text-center">
      <div class="col-xs-12">
        <section class="content">
          <div class="row">

              <!-- DONUT CHART -->
              <div class="box box-danger">
                <div class="box-header with-border">
                  <h3 class="box-title">KAS KELAS</h3>
                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div>
                <div class="box-body chart-responsive">
                  <canvas id="chart-area">
                </div>
                  <b class="text-warning">YELLOW</b> : Total Kas
                  <b class="success">GREEN</b> : Sisa Kas
                  <b class="text-danger">RED</b> : Kas Keluar

                <!-- /.box-body -->
              </div>
              <!-- /.box -->

            </div>
            <!-- /.col (LEFT) -->

        </section>
      </div>
      <br>
      <hr>
      <br>
      <div class="col-xs-12">
        <div class="col-md-4">
          <!-- Apply any bg-* class to to the info-box to color it -->
          <div class="info-box bg-aqua">
            <span class="info-box-icon"><i class="fa fa-comments-o"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">TOTAL KAS</span>
              <span class="info-box-number">Rp.{{$data->total_kas}},-</span>
              <!-- The progress section is optional -->
              <div class="progress">
                <div class="progress-bar" style="width: {{number_format($total,2)}}%"></div>
              </div>
              <span class="progress-description">
              {{number_format($total,2)}}% Selama
              </span>
            </div><!-- /.info-box-content -->
          </div><!-- /.info-box -->
        </div>
        <div class="col-md-4">
          <!-- Apply any bg-* class to to the info-box to color it -->
          <div class="info-box bg-red">
            <span class="info-box-icon"><i class="fa fa-comments-o"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">KAS KELUAR</span>
              <span class="info-box-number">Rp.{{$data->kas_keluar}},-</span>
              <!-- The progress section is optional -->
              <div class="progress">
                <div class="progress-bar" style="width: {{number_format($keluar,2)}}%"></div>
              </div>
              <span class="progress-description">
                {{number_format($keluar,2)}}% Selama
              </span>
            </div><!-- /.info-box-content -->
          </div><!-- /.info-box -->
        </div>
        <div class="col-md-4">
          <!-- Apply any bg-* class to to the info-box to color it -->
          <div class="info-box bg-green">
            <span class="info-box-icon"><i class="fa fa-comments-o"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">SISA KAS</span>
              <span class="info-box-number">Rp.{{$data->sisa_kas}},-</span>
              <!-- The progress section is optional -->
              <div class="progress">
                <div class="progress-bar" style="width: {{number_format($sisa,2)}}%"></div>
              </div>
              <span class="progress-description">
                {{number_format($sisa,2)}}% Selama
              </span>
            </div><!-- /.info-box-content -->
          </div><!-- /.info-box -->
        </div>
      </div>
    </div>
  </div>
  <br>
</div>
<br>
<div class="container p-4" >
  <div class="col-12">
    <h2 class="text-center">Info KASOnline</h2>
    <hr>
    <div class="row text-center">
      @if($best1->total_kas >0)
      <div class="col-xs-4">
        <h2 class="text-primary">{{($best1->nama)}}</h1>
        <b>Rp.{{$best1->total_kas}},-</b>
        <h4>The Best Class Money Storage</h4>
      </div>
        @else
        <div class="col-xs-4">
          <h2 class="text-primary">Jadilah Yang Pertama</h1>
          <b>Rp.xx-xx,-</b>
          <h4>The Best Class Money Storage</h4>
        </div>
        @endif
      @if($best2->kas_keluar >0)
      <div class="col-xs-4">
        <h2 class="text-primary">{{$best2->nama}}</h1>
        <b>Rp.{{$best2->kas_keluar}},-</b>
        <h4>The Best Class Money Outing</h4>
      </div>
        @else
        <div class="col-xs-4">
          <h2 class="text-primary">Jadilah Yang Pertama</h1>
          <b>Rp.xx-xx,-</b>
          <h4>The Best Class Money Outing</h4>
        </div>
        @endif
      @if($best3->user_bayar >0)
      <div class="col-xs-4">
        <h2 class="text-primary">{{$best3->nama}}</h1>
        <b>Rp.{{$best3->user_bayar}},-</b>
        <h4>The Best Student Money Payed</h4>
      </div>
      @else
      <div class="col-xs-4">
        <h2 class="text-primary">Jadilah Yang Pertama</h1>
        <b>Rp.xx-xx,-</b>
        <h4>The Best Student Money Payed</h4>
      </div>
      @endif
    </div>
  </div>
  <br>
</div>
@endsection
@section('js')
<script src="{{URL::asset('js/Chart.js')}}" type="text/javascript">

</script>
  <script type="text/javascript">
  var pieData = [
  				{
  					value: {{$data->sisa_kas}},
  					color:"green",
  					highlight: "green",
  					label: "SISA KAS"
  				},
  				{
  					value: {{$data->kas_keluar}},
  					color: "red",
  					highlight: "red",
  					label: "KAS KELUAR"
  				},
  				{
  					value: {{$data->total_kas}},
  					color: "blue",
  					highlight: "blue",
  					label: "KAS TOTAL"
  				}

  			];

  			window.onload = function(){
  				var ctx = document.getElementById("chart-area").getContext("2d");
  				window.myPie = new Chart(ctx).Pie(pieData);
  			};


  </script>
@endsection
