@extends('layouts.lte2')
@section('header')
Dashboard
@endsection
@section('css')
<style>

  .container{
    background: white;
    border-radius: 4px;
  }
  .space{
    background-color: #e4e3e3;
  }
</style>
@endsection
@section('content')
<?php $auth = \Auth::user(); ?>
<div class="container p-4" >
  <h1 class="text-center">SELAMAT DATANG {{$auth->nama}}</h1>
  <hr>
</div>
@endsection
@section('js')
<!-- <script type="text/javascript">
new Vue({
  data: {
  }
});
</script> -->
@endsection
