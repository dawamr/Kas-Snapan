@extends('layouts.lte2')
@section('css')
  <style>

    .container{
      background: white;
      border-radius: 4px;
    }
  </style>
@endsection
@section('header')
Sekolah Management
@endsection
@section('content')
<div class="container p-4" id="vue">
  <h1 class="text-center">KAS - ONLINE </h1>
  <h3 class="text-center">KA-<span class="text-success">LINE</span></h3>
  <hr>
  <div class="btn-group" role="group" aria-label="basic example">
    <button type="submit" class="btn btn-secondary btn-primary" name="button"><i class="fa fa-reply"></i> &nbsp Kembali</button>
    <button type="button" class="btn btn-secondary btn-success" name="button" onclick="location.href='/petugas'"><i class="fa fa-home"></i> &nbsp Beranda</button>
  </div>
<hr>
<table class="table">
  <tr>
    <th>Nama Kelas</th>
    <th>Jurusan</th>
    <th>Tahun Angkatan</th>
    <th>Aksi</th>
  </tr>

  @foreach($kelas as $mykelas)
  <tr>
    <td>{{$mykelas->nama}}</td>
    <td>{{$mykelas->jurusan}}</td>
    <td>

        <div class="btn-group" role="group" aria-label="basic example">
        <form class="form" action="/owner/db-kelas/{{$mykelas->id}}/edit" method="post">
          {{csrf_field()}}
          <button type="submit" class="btn btn-secondary btn-warning" name="button"><i class="fa fa-pencil"></i> Edit</button>
        </form>
      <form class="form" action="/owner/db-kelas/{{$mykelas->id}}" method="post">
        {{csrf_field()}}
          <button type="submit" class="btn btn-secondary btn-danger" name="button"><i class="fa fa-trash"></i> Hapus</button>
      </form>
        </div>
    </td>
  </tr>
  @endforeach

</table>
</div>
@endsection
@section('js')
@endsection
