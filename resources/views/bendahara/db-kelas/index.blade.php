
  @extends('layouts.lte')
  @section('css')
    <style>

      .container{
        background: white;
        border-radius: 4px;
      }
      #panel{
        background-color: rgb(232, 232, 232)
      }
      .card{
        background-color: rgb(255, 255, 255)
      }
    </style>
  @endsection
  @section('header')
  KAS MANAGEMENT
  @endsection
  @section('content')
  <div class="container p-4" >
    <h1 class="text-center">Panel Info</h1>
    <hr>
    <div class="row col-md-12">
      <div class="col-md-6 text-center">
        <h3>Update Kas</h3>
        <hr>
        <br>
        <?php
          $kas = \App\Kas::where('kelas_id', \Auth::user()->kelas_id)->first();
        ?>
        <form class="form" action="/petugas/kelas/{{$kelas->id}}/kas_mingguan" method="post">
          {{csrf_field()}}

            <button type="submit" class="btn btn-lg btn-danger" name="button"><i class="fa fa-plus"></i> &nbsp Update Kas
            </button>
        </form>
        <br>
        <hr>
      </div>

      <div class="col-md-6">
        <h3 class="text-center">Rincian Kas</h3>
        <hr>
        <table class="table">
          <tr>
            <td><b>Total Kas :</b></td>
            <td class="text-right">Rp. {{$kas->total_kas}},-</td>
            <td>
              <a href="#" class="btn btn-sm btn-primary"> Detail </a>
            </td>
          </tr>
          <tr>
            <td><b>Kewajiban Bayar :</b></td>
            <td class="text-right">Rp. {{$kas->kas_bayar}},-</td>
            <td>
              <a href="#" class="btn btn-sm btn-primary"> Detail </a>
            </td>
          </tr>
          <tr>
            <td><b>Total Pengeluaran :</b></td>
            <td class="text-right">Rp. {{$kas->kas_keluar}},-</td>
            <td>
              <a href="#" class="btn btn-sm btn-primary"> Detail </a>
            </td>
          </tr>
          <tr>
            <td><b>Kas Tersedia :</b></td>
            <td class="text-right">Rp. {{$kas->sisa_kas}},-</td>
            <td>
              <a href="#" class="btn btn-sm btn-primary"> Detail </a>
            </td>
          </tr>
        </table>
      </div>
    </div>
  </div>

  <br>
  <div class="container p-4" >
    <h1 class="text-center">Siswa {{$kelas->nama}}</h1>
    <hr>

  <div class="card">
    <table class="table">
      <tr>
        <th>No</th>
        <th>NIS</th>
        <th>Nama Siswa</th>
        <th>Status</th>
        <th>Dibayar</th>
        <th>Pengeluaran</th>
        <th>Opsi</th>
      </tr>
      <?php
        $no = 1;
          // $users = DB::table('users')->where('status', '=', 'user')->get();
      ?>

      @foreach($siswa as $mysiswa)
      <tr>
          <td>{{$no++}}</td>
          <td>{{$mysiswa->nis}}</td>
          <td>{{$mysiswa->nama}}</td>
          <td>
            @if($mysiswa->user_bayar >= $mysiswa->kas_bayar)
            <span class="text-success"><b> Lunas</b></span>
            @endif
            @if($mysiswa->user_bayar < $mysiswa->kas_bayar)
            <span class="text-danger"><b>Kurang Rp. {{($mysiswa->kas_bayar - $mysiswa->user_bayar)}},-</b></span>
            @endif
          </td>
          <td>Rp.{{$mysiswa->user_bayar}},-</td>
          <td>
            <?php
            $m = \App\Pengeluaran::where('user_id', $mysiswa->uid)->get();
              $n = 0;
            for ($i=0; $i <sizeof($m) ; $i++) {
              $n = $n + $m[$i]->uang_keluar;
            }
              echo "Rp. ".$n.",-";
             ?>
          </td>
          <td>
            <button type="button" class="btn btn-group btn-primary" name="button" onclick="location.href='/petugas/kelas/{{$mysiswa->id}}/detail'"><i class="fa fa-view"></i> Detail</button>
          </td>
      </tr>
      @endforeach
    </table>
    <br>
    <hr>
  </div>
  </div>
  @endsection
  @section('js')

  @endsection
