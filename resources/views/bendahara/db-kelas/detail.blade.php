
@extends('layouts.lte')
@section('css')
  <style>

    .container{
      background: white;
      border-radius: 4px;
    }
  </style>
@endsection
@section('header')
KAS MANAGEMENT
@endsection
@section('content')

<div class="container p-4" >
  <h2 id="pemasukan" class="text-center">Detail Pembayaran Kas <br> {{$siswa->nama}} </h2>
  <hr>
  <br>
  <br>
  @if($message = Session::get('success'))
  <div class="alert alert-success">
    <button type="button" class="close" data-dismiss="alert" name="button">x</button>
    <strong>{{$message}}</strong>
  </div>
<hr>
  @endif
  @if($message = Session::get('wrong'))
  <div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert" name="button">x</button>
    <strong>{{$message}}</strong>
  </div>
<hr>
  @endif
  <form class="form" action="/petugas/kelas/bayar-kas" method="post">
    {{csrf_field()}}
    <div class="btn-group" role="group" aria-label="basic example">
      <button type="button" class="btn btn-secondary btn-primary" name="button" onclick="location.href='/petugas/kelas'"><i class="fa fa-reply"></i> &nbsp Kembali</button>
      <button type="button" name="button" class="btn btn-secondary btn-danger" onclick="location.href='#pengeluaran'">Detail Pengeluaran</button>
      <button type="submit" class="btn btn-secondary btn-success" name="button"><i class="fa fa-home"></i> &nbsp Tambah Pembayaran</button>
    </div>
    <input type="number" name="user_id" value="{{$siswa->user_id}}" hidden>
  </form>

    <br><br>
    <h4>Total Pembayaran : <span class="text-danger">{{$siswa->user_bayar}}</span></h4>
  <table class="table">
    <tr>
      <th>No</th>
      <th>Jumlah bayar</th>
      <th>At</th>
      <th>Opsi</th>
    </tr>
    <?php
      $no = 1;
        // $users = DB::table('users')->where('status', '=', 'user')->get();
    ?>
    @foreach($detail as $mysiswa)
    <tr>
        <td>{{$no++}}</td>
        <td>{{$mysiswa->bayar}}</td>
        <td>
          <?php
            $updated_at = \Carbon\Carbon::parse($mysiswa->updated_at)->format('l, j F Y');
           ?>
          {{$updated_at}}
        </td>
        <td>
          <form class="" action="/petugas/kelas/{{$mysiswa->id}}/destroy" method="post">
          {{ csrf_field() }}
          <input type="number" name="pemasukan_id" value="{{$mysiswa->pemasukan_id}}" hidden>
            <button type="submit" class="btn btn-group btn-danger" name="button"><i class="fa fa-trash"></i> Hapus</button>
          </form>
        </td>
    </tr>
    @endforeach
  </table>
</div>
<br>
<div class="container p-4" >
  <h2 id="pengeluaran" class="text-center">Detail Pembayaran Kas <br> {{$siswa->nama}} </h2>
  <hr>
  <br>

    <div class="btn-group" role="group" aria-label="basic example">
      <button type="button" class="btn btn-secondary btn-primary" name="button" onclick="location.href='/petugas/kelas'"><i class="fa fa-reply"></i> &nbsp Kembali</button>
      <button type="button" class="btn btn-secondary btn-success" name="button"  onclick="location.href='#pemasukan'"><i class="fa fa-plus"></i> &nbsp Detail Pemasukan</button>
    </div>
    <?php
    $m = \App\Pengeluaran::where('user_id', $siswa->user_id)->get();
      $n = 0;
    for ($i=0; $i <sizeof($m) ; $i++) {
      $n = $n + $m[$i]->uang_keluar;
    }
     ?>
    <br><br>
    <h4>Total Pengeluaran : <span class="text-danger">{{$n}}</span></h4>
  <table class="table">
    <tr>
      <th>No</th>
      <th>Jumlah Keluar</th>
      <th>Keterangan</th>
      <th>At</th>
      <th>Opsi</th>
    </tr>
    <?php
      $no = 1;
        // $users = DB::table('users')->where('status', '=', 'user')->get();
    ?>
    @foreach($m as $pengeluaran)
    <tr>
        <td>{{$no++}}</td>
        <td>{{$pengeluaran->uang_keluar}}</td>
        <td>
          <textarea name="name" rows="auto"cols="auto" disabled>{{$pengeluaran->ket}}</textarea>
        </td>
        <td>
          <?php
            $updated_at = \Carbon\Carbon::parse($pengeluaran->updated_at)->format('l, j F Y');
           ?>
          {{$updated_at}}</td>
        <td>
          <form class="" action="/petugas/kelas/x/{{$pengeluaran->id}}/destroy" method="post">
          {{ csrf_field() }}
          <input type="hidden" name="uang_keluar" value="{{$pengeluaran->uang_keluar}}">
          <input type="number" name="user_id" value="{{$pengeluaran->user_id}}" hidden>
            <button type="submit" class="btn btn-group btn-danger" name="button"><i class="fa fa-trash"></i> Hapus</button>
          </form>
        </td>
    </tr>
    @endforeach
  </table>
</div>
@endsection
@section('js')

@endsection
