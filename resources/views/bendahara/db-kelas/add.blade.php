@extends('layouts.lte')
@section('css')
  <style>

    .container{
      background: white;
      border-radius: 4px;
    }
  </style>
@endsection
@section('header')
KAS MANAGEMENT
@endsection
@section('content')

<div class="container p-4">
  <h1 class="text-center">Pembayaran {{$siswa->nama}}</h1><br>

  <form class=""  id="myform" onsubmit="return validasi()" action="/petugas/kelas/save" method="post">
<?php $now = date('Y-m-d') ?>
    {{ csrf_field() }}
  <div class="row">

      <div class="col-xs-6 form-group">
        <label>Bayar</label>
        <input type="number" name="bayar" id="bayar" placeholder="RP. " class="form-control" required><br>
      </div>
      <!-- <div class="col-xs-4 form-group">
        <label>Tanggal</label>
        <input type="date" class="form-control" name="date" value="{{$now}}" required><br>
      </div> -->

      <input type="number" name="user_id" value="{{$siswa->id}}" hidden>
      <input type="number" name="pemasukan_id" value="{{$siswa->pemasukan_id}}" hidden>
      <input type="number" name="user_bayar" value="{{$siswa->user_bayar}}" hidden>
  </div>
    <input type="submit" name="submit" value="Bayar Kas" class="btn btn-md btn-primary">
    <a href="/petugas/kelas/{{$siswa->pemasukan_id}}/detail" class="btn btn-md btn-danger">Batalkan</a>
  </form><br><br>
</div>
@endsection
@section('js')
<script type="text/javascript">
  function validasi(){
    var bayar = document.forms["myform"]["bayar"].value,
    var numbers = /^[0-9]+$/,

    if (bayar==null || bayar==""){
      alert("Maaf, nominal harus di isi."),
      return false,
    }
    if (!bayar.match(numbers)) {
      alert("Maaf, nominal harus angka."),
      return false,
    }
    if (nip.length!<3) {
      alert("Maaf, nominal pembayaran mininal Rp. 1000,-"),
      return false,
    }
  }
</script>
@endsection
