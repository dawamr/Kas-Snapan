
@extends('layouts.lte')
@section('css')
  <style>
    .container{
      background: white;
      border-radius: 4px;
    }
  </style>
@endsection
@section('header')
KAS MANAGEMENT
@endsection
@section('content')

<div class="container p-4" >
  <h1 class="text-center">Siswa</h1>
<br>
<?php
  $no = 1;
    // $users = DB::table('users')->where('status', '=', 'user')->get();
  $now = date('Y-m-d')
?>
<br>
    @if($message = Session::get('success'))
    <div class="alert alert-success">
      <button type="button" class="close" data-dismiss="alert" name="button">x</button>
      <strong>{{$message}}</strong>
    </div>
    <hr>
    @endif
    @if($message = Session::get('wrong'))
    <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert" name="button">x</button>
      <strong>{{$message}}</strong>
    </div>
    <hr>
    @endif
  <form class="form" action="/petugas/kas" method="post">
    {{csrf_field()}}
  <div class="btn-group" role="group" aria-label="basic example">
    <button type="submit" class="btn btn-secondary btn-primary" name="button"><i class="fa fa-save"></i> &nbsp Simpan Data</button>
    <button type="button" class="btn btn-secondary btn-danger" onclick="location.href='/petugas/kas/keluar'" name="button"><i class="fa fa-close"></i> &nbsp KAS keluar</button>
    <button type="button" class="btn btn-secondary btn-success" name="button" onclick="location.href='/petugas'"><i class="fa fa-home"></i> &nbsp Beranda</button>
</div>
<hr>
  <table class="table">
    <!-- <tr>
      <td>
        <div class="row">
          <div class="col-xs-2">Tanggal :</div><div class="col-xs-4"><input type="date" class="form-control" name="date" value="{{$now}}" required></div>
        </div>
      </td>
    </tr> -->
    <tr>
      <th class="text-center">Nama</th>
      <th class="text-center">KAS Masuk</th>
      <th class="text-center">Status</th>
    </tr>
    <tr>
      <td>Pemasukan Lainnya</td>
      <td>
        <input type="number" name="bayar2" class="form-control" value="">
      </td>
    </tr>
    @foreach($siswa as $mysiswa)
    <tr>
        <td>
          {{$mysiswa->nama}}
          <input min="0" type="number" name="id[]" value="{{$mysiswa->id}}" hidden>
          <input min="0" type="number" name="iduser[]" value="{{$mysiswa->iduser}}" hidden>
          <input min="0" type="number" name="idkas[]" value="{{$mysiswa->idkas}}" hidden>
          <input min="0" type="number" name="user_bayar[]" value="{{$mysiswa->user_bayar}}" hidden>
        </td>
        <td><input type="number" name="bayar[]" id="bayar[]" class="form-control" value=""></td>
        <td class="text-center">
          @if($mysiswa->user_bayar >= $mysiswa->kas_bayar)
          <span class="text-success" caption="Lunas"><b>Tersedia Rp.{{($mysiswa->user_bayar - $mysiswa->kas_bayar)}}</b></span>
          @endif
          @if($mysiswa->user_bayar < $mysiswa->kas_bayar)
          <span class="text-danger"><b>Kurang Rp.{{($mysiswa->kas_bayar - $mysiswa->user_bayar)}}</b></span>
          @endif
        </td>
    </tr>
    @endforeach
  </form>
  </table>
</div>
@endsection
@section('js')
<script type="text/javascript">
  function ValidatorQ(){
    var BAYAR[] = document.getElementById('bayar[]');
		if(isNumeric(BAYAR[], "Masukan hanya numerik")){
			if(lengthRestriction(BAYAR, 9,9)) {
			}
		}

	return false;
  }
  function lengthRestriction(elem, min, max){
	var uInput = elem.value;
	if(uInput.length >= min && uInput.length <= max){
		return true;
	}else{
		alert("Please enter between " +min+ " and " +max+ " characters");
		elem.focus();
		return false;
	}
}
function isNumeric(elem, helperMsg){
	var numericExpression = /^[0-9]+$/;
	if(elem.value.match(numericExpression)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
}

</script>
@endsection
