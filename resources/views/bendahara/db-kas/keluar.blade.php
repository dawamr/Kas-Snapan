@extends('layouts.lte')
@section('css')
  <style>

    .container{
      background: white;
      border-radius: 4px;
    }
  </style>
@endsection
@section('header')
KAS MANAGEMENT
@endsection
@section('content')

<div class="container p-4">
  <h1 class="text-center">Laporan KAS Keluar</h1><br>
  <hr>
  @if($message = Session::get('success'))
  <div class="alert alert-success">
    <button type="button" class="close" data-dismiss="alert" name="button">x</button>
    <strong>{{$message}}</strong>
  </div>
<hr>
  @endif
  @if($message = Session::get('wrong'))
  <div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert" name="button">x</button>
    <strong>{{$message}}</strong>
  </div>
<hr>
  @endif
  <form class="form" action="/petugas/kas/keluar" method="post">
    {{csrf_field()}}
    <div class="btn-group" role="group" aria-label="basic example">
      <button type="button" class="btn btn-secondary btn-primary" onclick="location.href='/petugas/kas'" name="button"><i class="fa fa-reply"></i> &nbsp Kembali</button>
      <button type="submit" class="btn btn-secondary btn-success" name="button"><i class="fa fa-save"></i> &nbsp Simpan Data</button>
      <button type="button" class="btn btn-secondary btn-danger" name="button" onclick="location.href='/petugas'"><i class="fa fa-home"></i> &nbsp Beranda</button>
    </div>
    <br>
    <br>
    <div class="col-md-12">
      <div class="col-md-4">
        <br>
            <label>Nama Peminjam</label>
            <select class="form-control" id="user_id" name="user_id">
              <option value="">Nama Peminjam</option>
              @foreach($data as $datas)
                <option value="{{$datas->id}}"> {{$datas->nama}}</option>
                <?php
                  $id = $datas->pemasukan_id;
                 ?>
              @endforeach
            </select>

            <span class="text-grey" style="font-size: 11px">*Kosongkan jika kas digunakan untuk keperluan kelas.</span>
     </div><br>

      <div class="col-md-6 form-group">
          <label>Nominal KAS Keluar</label>
          <input type="number" name="keluar" min="0" max="$mysiswa->user_bayar >= $mysiswa->kas_bayar" id="keluar" placeholder="RP. " class="form-control" required><br>
          <label>Keterangan</label>
          <textarea class="form-control" name="keterangan" rows="8" cols="80" required placeholder="Keterangan Meminjam"></textarea><br>
      </div>
    </div>
    <input type="number" name="pemasukan_id" value="{{$id}}" hidden>
  </form>

  <br>
  <hr>
</div>
@endsection
@section('js')

@endsection
