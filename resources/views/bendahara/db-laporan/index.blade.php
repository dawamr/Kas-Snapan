
@extends('layouts.lte')
@section('css')
  <style>

    .container{
      background: white;
      border-radius: 4px;
    }
  </style>
@endsection
@section('header')
Laporan Dan Download KAS Kelas
@endsection
@section('content')
<div class="container p-4" >
  <h1 class="text-center">Laporan dan Download</h1>
  <hr>

  <table class="table">
  <tr>
    <th>No.</th>
    <th>Bulan</th>
    <th>Total Pemasukan (/bln)</th>
    <th>Total Pengeluaran (/bln)</th>
    <th>Aksi</th>
    <th> &nbsp</th>
  </tr>
  <?php $no = 01;?>
  @foreach($bulan as $mybulan)
  <tr>
    <td>{{$no++}}</td>
    <td>{{$mybulan}}</td>
    <td><?php
    $m = \App\User::where('kelas_id', \Auth::user()->kelas_id)
          ->join('pemasukans','users.id','=','pemasukans.user_id')
          ->join('detail_pemasukans','pemasukans.id','=','detail_pemasukans.pemasukan_id')
          ->select('detail_pemasukans.bayar as bayar','detail_pemasukans.updated_at as updated_at')
          ->get();
      // dd($m);
      $n = 0;
      $updated_at = Now();
      for ($i=0; $i <sizeof($m) ; $i++) {
        $x[$i] = \Carbon\Carbon::parse($m[$i]->updated_at)->format('m');
        $y[$i] = \Carbon\Carbon::parse($m[$i]->updated_at);
        if ($x[$i] == $no-1) {
          $v = $m->where('updated_at', $y[$i]);
          $n = $n + $v[$i]->bayar;
        }
      }
      echo  "Rp. ".$n.",-";
      ?>
    </td>
    <td>
      <?php
        $m = \App\Pengeluaran::join('users','pengeluarans.user_id','=','users.id')
              ->where('kelas_id', \Auth::user()->kelas_id)
             ->get();
          $n = 0;
          for ($i=0; $i < sizeof($m) ; $i++) {
            $x[$i] = \Carbon\Carbon::parse($m[$i]->updated_at)->format('m');
            $y[$i] = \Carbon\Carbon::parse($m[$i]->updated_at);
            if ($x[$i] == $no-1) {
              $v = $m->where('updated_at', $y[$i]);
              $n = $n + $v[$i]->uang_keluar;
            }
          }
          echo "Rp. ".$n.",-";
       ?>
    </td>
    <td>
      <form class="form" action="/petugas/laporan/detail" method="post">
        {{csrf_field()}}
        <input type="number" name="id" value="{{$no-1}}" hidden>

        <button type="submit" class="btn btn-md btn-warning" name="button">Detail</button>
      </form>
    </td>
    <td></td>
  </tr>
  @endforeach
  </table>

</div>

@endsection
@section('js')

@endsection
