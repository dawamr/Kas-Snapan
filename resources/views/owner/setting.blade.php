
@extends('layouts.lte')
@section('css')
  <style>

    .container{
      background: white;
      border-radius: 4px;
    }
    .break{
      background: #e4e3e3;
    }
  </style>
@endsection
@section('header')
Pengaturan KAS Kelas
@endsection
@section('content')
  @php
    $user = \Auth::user();
  @endphp
  <div class="container p-4">
    <h1 class="text-center">Pengaturan Kas Kelas</h1>
    <hr>
    <div class="col-md-12">
      <div class="col-md-6">
        <div class="card">
          <h3 class="text-center">Detail Akun</h3>
          <hr>
          <br>
          @if($message  = Session::get('success1'))
          <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert" name="button">x</button>
            <strong>{{$message}}</strong>
          </div>
          <hr>
          @endif
          @if($message  = Session::get('wrong1'))
          <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert" name="button">x</button>
            <strong>{{$message}}</strong>
          </div>
          @endif
          <form class="form" action="/owner/set-account" method="post">
            @csrf
            <table class="table">
              <tr>
                <td width="30%" style="font-weight: bold">Nama</td>
                <td width="5%" > : </td>
                <td width="65%" >
                <input class="form-control" type="text" name="nama" placeholder="{{$user->nama}}">
                </td>
              </tr>
              <tr>
                <td width="30%" style="font-weight: bold">Email</td>
                <td width="5%" > : </td>
                <td width="65%" >
                <input class="form-control" type="text" name="email" placeholder="{{$user->email}}">
                </td>
              </tr>
              <tr>
                <td>
                  <a onclick="openPass()">Ganti Password</a>
                </td>
              </tr>
              <tr>
                <td width="30%" style="font-weight: bold">New Password</td>
                <td width="5%" > : </td>
                <td width="65%" >
                <input class="form-control" id="myPass1" type="password" name="password" value="" disabled>
                </td>
              </tr>
              <tr>
                <td width="30%" style="font-weight: bold">Retyping Password</td>
                <td width="5%" > : </td>
                <td width="65%" >
                <input class="form-control" id="myPass2" type="password" name="rePassword" value="" disabled>
                </td>
              </tr>
              </table>
              <div class="btn-group" role="group" aria-label="basic example">
                <button type="submit" onclick="required()" class="btn btn-secondary btn-primary" name="button"><i class="fa fa-pencil"></i> &nbsp Simpan</button>
                  <a type="button" class="btn btn-secondary btn-danger" onclick="closePass()"> <i class="fa fa-close"></i> &nbsp Batal</a>
                <br>
              </div>
          </form>
        </div>
        <br>
      </div>
      <div class="col-md-6">
        <div class="card">
          <h3 class="text-center">Ganti Nama Aplikasi</h3>
          <hr>
          @if($message  = Session::get('success2'))
          <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert" name="button">x</button>
            <strong>{{$message}}</strong>
          </div>
          <hr>
          @endif
          @if($message  = Session::get('wrong2'))
          <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert" name="button">x</button>
            <strong>{{$message}}</strong>
          </div>
          <hr>
          @endif
          <form action="/owner/set-app" method="post" class="form">
            {{ csrf_field() }}
            <div class="row">
              <br>
              <div class="col-md-5">
                <label for="">Nama Aplikasi</label>
                <input type="text" class="form-control" placeholder="KAS ONLINE" name="AppName" value="" required>
              </div>
              <div class="col-md-5">
                <label for="">Singkatan Aplikasi</label>
                <input type="text" class="form-control" placeholder="KALINE" name="AppName1" value="">
              </div>
            </div>
            <span>*Jika ingin mengganti nama aplikasi.</span>
            <br><br>
            <button type="submit" class="btn btn-md btn-primary" name="button">Simpan</button>
          </form>
          <br>
          </div>
        </div>
      </div>
    </div>
    <div class="break">
      <br>
    </div>

@endsection
@section('js')
  <script type="text/javascript">
    function openPass(){
      document.getElementById('myPass1').disabled = false;
      document.getElementById('myPass2').disabled = false;
    }
    function closePass(){
      document.getElementById('myPass1').disabled = true;
      document.getElementById('myPass2').disabled = true;
      document.getElementById('myPass1').value = "";
      document.getElementById('myPass2').value = "";
    }
  </script>
@endsection
