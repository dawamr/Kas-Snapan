@extends('layouts.lte')
@section('css')
  <style>

    .container{
      background: white;
      border-radius: 4px;
    }
  </style>
@endsection
@section('header')
Tambah Sekolah
@endsection
@section('content')
<div class="container p-4">
  <h1 class="text-center">Tambah Sekolah</h1>
  <hr>
  <form class="" action="/owner/db-sekolah" method="post">
    {{ csrf_field() }}
  <div class="row">

      <div class="col-lg-6 form-group">
        <label>Nama</label>
        <input type="text" name="nama" placeholder="SMA/K/MA NamaSekolah Semarang" class="form-control"><br>
        <label>Alamat</label>
        <input type="text" name="alamat" placeholder="Jl. Alamat Sekolah, Kec. ,Kota" class="form-control"><br>
        <label>Email</label>
        <input type="text" name="email" placeholder="EmailSekolah@email.com" class="form-control">
      </div>

  </div><br>
    <input type="submit" name="submit" value="Tambah Sekolah" class="btn btn-md btn-primary">
    <a href="/owner/db-sekolah" class="btn btn-md btn-danger">Batalkan</a>
  </form><br><br>
</div>
@endsection
