
@extends('layouts.lte')
@section('css')
  <style>

    .container{
      background: white;
      border-radius: 4px;
    }
  </style>
@endsection
@section('header')
Sekolah Management
@endsection
@section('content')
<div class="container p-4" id="vue">
  <h1 class="text-center">KAS - ONLINE </h1>
  <h3 class="text-center">KA-<span class="text-success">LINE</span></h3>
  <hr>
      <a class="btn btn-md btn-success" href="/owner/db-sekolah/add" >Tambah Sekolah</a><br><br>
    <?php
      $tahun = date("Y");
     ?>
  <table class="table">
    <tr>
      <th>No</th>
      <th>Nama Sekolah</th>
      <th>Alamat</th>
      <th>Email</th>
      <th>Opsi</th>
    </tr>
    <?php
      $no = 1;
        // $users = DB::table('users')->where('status', '=', 'user')->get();
    ?>
    @foreach($sekolah as $mysekolah)
    <tr>
        <td>{{$no++}}</td>
        <td>{{$mysekolah->nama}}</td>
        <td>{{$mysekolah->alamat}}</td>
        <td>{{$mysekolah->email}}</td>
        <td>
          <form class="form" action="/owner/db-sekolah/{{$mysekolah->id}}" method="post">
            {{csrf_field()}}
            <div class="btn-group" role="group" aria-label="basic example">
              <button type="button" class="btn btn-secondary btn-warning" name="button" onclick="location.href='/owner/db-sekolah/{{$mysekolah->id}}/edit'"><i class="fa fa-pencil"></i> Edit</button>
              <button type="button" class="btn btn-secondary btn-success" name="button" onclick="location.href='/owner/db-sekolah/{{$mysekolah->id}}/view'"><i class="fa fa-circle"></i> Lihat</button>
              <button type="submit" class="btn btn-secondary btn-danger" name="button"><i class="fa fa-trash"></i> Hapus</button>
            </div>
          </form>
        </td>
    </tr>
    @endforeach
  </table>
</div>
@endsection
@section('js')
@endsection
