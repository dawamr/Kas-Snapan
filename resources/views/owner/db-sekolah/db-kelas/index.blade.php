
@extends('layouts.lte')
@section('css')
  <style>

    .container{
      background: white;
      border-radius: 4px;
    }
  </style>
@endsection
@section('header')
Sekolah Management
@endsection
@section('content')

<div class="container p-4" id="vue">
  <h1 class="text-center">KAS - ONLINE </h1>
  <h3 class="text-center">KA-<span class="text-success">LINE</span></h3>
  <hr>
  <div class="btn-group" role="group" aria-label="basic example">
    <button type="submit" class="btn btn-secondary btn-primary" name="button" onclick="location.href='/owner/db-sekolah'"><i class="fa fa-reply"></i> &nbsp Kembali</button>
    <button type="submit" class="btn btn-secondary btn-warning" name="button" onclick="location.href='/owner/{{$idsekolah}}/db-kelas/add'"><i class="fa fa-reply"></i> &nbsp Tambah Kelas</button>
    <button type="button" class="btn btn-secondary btn-success" name="button" onclick="location.href='/owner'"><i class="fa fa-home"></i> &nbsp Beranda</button>
  </div>
<hr>
<table class="table">
  <tr>
    <th>Nama Kelas</th>
    <th>Jumlah Siswa</th>
    <th>Jurusan</th>
    <th>Tahun Angkatan</th>
    <th>Aksi</th>
  </tr>
  @foreach($data as $datas)
  <tr>
    <td>{{$datas->nama}}</td>
    <?php
      $jml = \App\User::where('kelas_id', $datas->id)->join('role_user','user_id','=','users.id')
      ->where('role_user.role_id','<',4)->where('role_user.role_id','>',1)->get();
     ?>
    <td>{{count($jml)}}</td>
    <td>{{$datas->jurusan}}</td>
    <?php
      $ta = \App\Tahun::where('id', $datas->tahun_id)->first();
     ?>
    <td>{{$ta->tahun1}}-{{$ta->tahun2}}</td>
    <td>

        <div class="btn-group" role="group" aria-label="basic example">
        <form class="form" action="/owner/db-kelas/{{$datas->id}}/edit" method="post">
          {{csrf_field()}}
          <input type="number" name="sekolah_id" value="{{$idsekolah}}" hidden>
          <button type="submit" class="btn btn-secondary btn-warning" name="button"><i class="fa fa-pencil"></i> Edit</button>
        </form>
      <form class="form" action="/owner/db-kelas/{{$datas->id}}/destroy" method="post">
        {{csrf_field()}}
          <button type="submit" class="btn btn-secondary btn-danger" name="button"><i class="fa fa-trash"></i> Hapus</button>
      </form>
        </div>
    </td>
  </tr>
  @endforeach
</table>
<li>{{$data->links()}}</li>
</div>
@endsection
@section('js')
@endsection
