@extends('layouts.lte')
@section('css')
  <style>

    .container{
      background: white;
      border-radius: 4px;
    }
  </style>
@endsection
@section('header')
Tambah Kelas
@endsection
@section('content')
<div class="container p-4">
  <h1 class="text-center">Tambah Kelas</h1>
  <hr>
  <form class="" action="/owner/db-kelas/store" method="post">
    {{ csrf_field() }}
  <div class="row">
  <input type="number" name="sekolah_id" value="{{$idsekolah}}" hidden>
      <div class="col-lg-6 form-group">
        <label>Nama Kelas</label>
        <input type="text" name="nama" placeholder="Nama Kelas" class="form-control"><br>
        <label>Tahun Angkatan</label>
          <?php $tahuns = \App\Tahun::get();?>
            <select class="form-control" id="tahun_id" name="tahun_id">
              @foreach($tahuns as $tahun)
                <option value="{{$tahun->id}}">{{$tahun->tahun1}} - {{$tahun->tahun2}}</option>
              @endforeach
            </select>
            <br>

        <label>Jurusan</label>
        <input type="text" name="jurusan" placeholder="Jurursan Kelas" class="form-control">
      </div>

  </div><br>
    <input type="submit" name="submit" value="Simpan Perubahan" class="btn btn-md btn-primary">
    <a href="/owner/db-sekolah/{{$idsekolah}}/view" class="btn btn-md btn-danger">Batalkan</a>
  </form><br><br>
</div>
@endsection
