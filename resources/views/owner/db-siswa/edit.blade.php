@extends('layouts.lte')
@section('css')
  <style>

    .container{
      background: white;
      border-radius: 4px;
    }
  </style>
@endsection
@section('header')
Edit Siswa
@endsection
@section('content')
<div class="container p-4">
  <h1 class="text-center">Edit Siswa</h1>
  <hr>
  <form class="" action="/owner/db-siswa/{{$siswa->id}}/update" method="post">
    {{ csrf_field() }}
  <div class="row">

    <?php
      $sekolah = \App\User::find($siswa->id)->join('kelas','users.kelas_id','=','kelas.id')
      ->select('kelas.sekolah_id as sekolah_id')
      ->first();
     ?>
      <input type="number" name="sekolah_id" value="{{$sekolah->sekolah_id}}" hidden>
      <div class="col-md-6 form-group">
        <label>Nama</label>
        <input type="text" name="nama" value="{{$siswa->nama}}" placeholder="Nama Siswa" class="form-control"><br>
        <label>NIS</label>
        <input type="number" name="nis" value="{{$siswa->nis}}" placeholder="NIS" class="form-control"><br>
        <label>Email</label>
        <input type="text" name="email" value="{{$siswa->email}}" placeholder="Email Siswa" class="form-control"><br>
        <label>Passoword</label>
        <input type="password" name="passoword" value="" placeholder="New Passoword" class="form-control">
      </div>
  </div><br>
    <input type="submit" name="submit" value="Simpan Perubahan" class="btn btn-md btn-primary">
    <a href="/owner/db-siswa" class="btn btn-md btn-danger">Batalkan</a>
  </form><br><br>
</div>
@endsection
