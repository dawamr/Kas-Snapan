@extends('layouts.lte')
@section('css')
  <style>

    .container{
      background: white;
      border-radius: 4px;
    }
  </style>
@endsection
@section('header')
Tamnah Siswa
@endsection
@section('content')
<div class="container p-4">
  <h1 class="text-center">Tambah Siswa</h1>
  <hr>
  
  <form class="" action="/owner/db-siswa/new/{{$id}}" method="post">
    {{ csrf_field() }}
  <div class="row">

      <div class="col-md-6 form-group">
        <label>Nama</label>
        <input type="text" name="nama" placeholder="Nama Siswa" class="form-control"><br>
        <label>NIS</label>
        <input type="number" name="nis" placeholder="NIS" class="form-control"><br>
        <label>Email</label>
        <input type="text" name="email" placeholder="Email Siswa" class="form-control"><br>
        <label>Passoword</label>
        <input type="password" name="passoword" value="" placeholder="New Passoword" class="form-control">
      </div>
      <div class="col-md-6 form-group">
        <label>Sekolah</label>
          <select class="form-control" id="kelas_id" name="kelas_id" required>
              <option value="">Pilih Sekolah</option>
              <?php $kelas= \App\Kelas::orderBy('nama', 'asc')->get() ?>
              @foreach($kelas as $kelas)
                  <option value="{{$kelas->id}}"> {{$kelas->nama}}</option>
              @endforeach
          </select>
          <label>Hak Akses</label>
            <select class="form-control" id="role_id" name="role_id" required>
                <option value="">Pilih Hak Akses</option>
                    <option value="2"> Bendahara</option>
                    <option value="3"> Siswa</option>
            </select>
        <br>
      </div>
  </div><br>
    <input type="submit" name="submit" value="Simpan Perubahan" class="btn btn-md btn-primary">
    <a href="/owner/db-siswa" class="btn btn-md btn-danger">Batalkan</a>
  </form><br><br>
</div>
@endsection
