
@extends('layouts.lte')
@section('css')
  <style>

    .container{
      background: white;
      border-radius: 4px;
    }
  </style>
@endsection
@section('header')
User Management
@endsection
@section('content')

<div class="container p-4" >
  <h1 class="text-center">Siswa</h1>
  <hr>
<a class="btn btn-md btn-success" href="/owner/db-siswa/{{$id}}/add" >Tambah Siswa</a><br><br>
  <table class="table">
    <tr>
      <th>No</th>
      <th>NIS</th>
      <th>Nama Siswa</th>
      <th>Email</th>
      <th>Kelas</th>
      <th>Opsi</th>
    </tr>
    <?php
      $no = 1;
        // $users = DB::table('users')->where('status', '=', 'user')->get();
    ?>
    @foreach($siswa as $mysiswa)
    <tr>
        <td>{{$no++}}</td>
        <td>{{$mysiswa->nis}}</td>
        <td>{{$mysiswa->nama}}</td>
        <td>{{$mysiswa->email}}</td>
        <td><b>{{$mysiswa->nama_kelas}}</b></td>
        <td>
          <form class="form" action="/owner/db-siswa/{{$mysiswa->id}}" method="post">
            {{csrf_field()}}
            <div class="btn-group" role="group" aria-label="basic example">
              <button type="button" class="btn btn-secondary btn-warning" name="button" onclick="location.href='/owner/db-siswa/{{$mysiswa->id}}/edit'"><i class="fa fa-pencil"></i> Edit</button>
              <button type="submit" class="btn btn-secondary btn-danger" name="button"><i class="fa fa-trash"></i> Hapus</button>
            </div>
          </form>
        </td>
    </tr>
    @endforeach
  </table>
</div>
@endsection
@section('js')

@endsection
