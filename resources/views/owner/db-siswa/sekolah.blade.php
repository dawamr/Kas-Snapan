
@extends('layouts.lte')
@section('css')
  <style>

    .container{
      background: white;
      border-radius: 4px;
    }
  </style>
@endsection
@section('header')
Asal Sekolah
@endsection
@section('content')
<div class="container p-4" id="vue">
  <h1 class="text-center">KAS - ONLINE </h1>
  <h3 class="text-center">KA-<span class="text-success">LINE</span></h3>
<hr><br>
    <?php
      $tahun = date("Y");
     ?>
  <table class="table">
    <tr>
      <th>No</th>
      <th>Nama Sekolah</th>
      <th>Lihat</th>
    </tr>
    <?php
      $no = 1;
        // $users = DB::table('users')->where('status', '=', 'user')->get();
    ?>
    @foreach($sekolah as $mysekolah)
    <tr>
        <td>{{$no++}}</td>
        <td>{{$mysekolah->nama}}</td>
        <td>
          <a class="btn btn-primary" href="/owner/db-siswa/{{$mysekolah->id}}"><i class="fa fa-view"></i> Lihat</a>
        </td>
    </tr>
    @endforeach
  </table>
</div>
@endsection
@section('js')
@endsection
