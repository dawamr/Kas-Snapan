@extends('layouts.lte')
@section('css')
  <style>

    .container{
      background: white;
      border-radius: 4px;
    }
  </style>
@endsection
@section('header')
Edit Guru
@endsection
@section('content')
<div class="container p-4">
  <h1 class="text-center">Edit Guru</h1>
  <hr>
  <form class="" action="/owner/db-guru/{{$guru->id}}/update" method="post">
    {{ csrf_field() }}
  <div class="row">

      <div class="col-md-6 form-group">
        <label>Nama</label>
        <input type="text" name="nama" value="{{$guru->nama}}" placeholder="Nama Guru" class="form-control"><br>
        <label>Email</label>
        <input type="text" name="email" value="{{$guru->email}}" placeholder="Email Guru" class="form-control"><br>
        <label>Passoword</label>
        <input type="password" name="passoword" value="" placeholder="New Passoword" class="form-control">
      </div>
  </div><br>
    <input type="submit" name="submit" value="Simpan Perubahan" class="btn btn-md btn-primary">
    <a href="/owner/db-guru" class="btn btn-md btn-danger">Batalkan</a>
  </form><br><br>
</div>
@endsection
