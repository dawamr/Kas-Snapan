
@extends('layouts.lte')
@section('css')
  <style>

    .container{
      background: white;
      border-radius: 4px;
    }
  </style>
@endsection
@section('header')
User Management
@endsection
@section('content')
<div class="container p-4" id="vue">
  <h1 class="text-center">KAS - ONLINE </h1>
  <h3 class="text-center">KA-<span class="text-success">LINE</span></h3>
      <a class="btn btn-md btn-success" href="/owner/sekolah/add" >Tambah Sekolah</a><br><br>
    <?php
      $tahun = date("Y");
     ?>
  <table class="table">
    <tr>
      <th>No</th>
      <th>Nama Sekolah</th>
      <th>Alamat</th>
      <th>Email</th>
      <th>Opsi</th>
    </tr>
    <?php
      $no = 1;
        // $users = DB::table('users')->where('status', '=', 'user')->get();
    ?>
    @foreach($sekolah as $mysekolah)
    <tr>
        <td>{{$no++}}</td>
        <td>
          <form class="form" action="/owner/angkatan/{{$tahun}}" method="post">
            {{csrf_field()}}
            <input type="number" name="idsekolah" value="{{$mysekolah->id}}" hidden>
            {{$mysekolah->nama}}
        </td>
        <td>{{$mysekolah->alamat}}</td>
        <td>{{$mysekolah->email}}</td>
        <td>
          <button type="submit" class="btn btn-sm btn-primary" name="button"><i class="fa fa-book"></i>&nbsp View</button>
          <button type="button" class="btn btn-sm btn-warning" name="button" onclick="location.href='sekolah/{{$mysekolah->id}}/edit'"><i class="fa fa-pencil"></i> Edit</button>
        </td>
        </form>
    </tr>
    @endforeach
  </table>
</div>
@endsection
@section('js')
<!-- <script type="text/javascript">
new Vue({
  Vue.component('kelas',{
        template: '#kelas-template',
      });
  data: {
    el: #vue,
      currentView : 'kelas',
    kelas: []
  },
  created:function(){
    this.readKelas()
  },
  methods : {
    readKelas(){
      var vm = this
      axios.get('/owner/api/kelas')
        .then(function (response) {
        })
    },
  }

});
</script> -->
@endsection
