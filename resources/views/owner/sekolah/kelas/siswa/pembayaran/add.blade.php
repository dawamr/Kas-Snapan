@extends('layouts.lte')
@section('css')
  <style>

    .container{
      background: white;
      border-radius: 4px;
    }
  </style>
@endsection
@section('header')
Tambah User
@endsection
@section('content')

<div class="container p-4">
  <h1 class="text-center">Pembayaran {{$siswa->nama}}</h1><br>
  <form class="" action="/owner/siswa/{{$siswa->pemasukan_id}}" method="post">
    {{ csrf_field() }}
  <div class="row">

      <div class="col-lg-6 form-group">
        <label>Bayar</label>
        <input type="number" name="bayar" placeholder="RP. " class="form-control"><br>
      </div>
      <input type="number" name="user_id" value="{{$siswa->id}}" hidden>
      <input type="number" name="kelas_id" value="{{$siswa->kelas_id}}" hidden>
      <input type="number" name="pemasukan_id" value="{{$siswa->pemasukan_id}}" hidden>
      <input type="number" name="user_bayar" value="{{$siswa->user_bayar}}" hidden>
  </div>
    <input type="submit" name="submit" value="Bayar Kas" class="btn btn-md btn-primary">
    <a href="/owner/sekolah" class="btn btn-md btn-danger">Batalkan</a>
  </form><br><br>
</div>
@endsection
