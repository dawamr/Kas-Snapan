
@extends('layouts.lte')
@section('css')
  <style>

    .container{
      background: white;
      border-radius: 4px;
    }
  </style>
@endsection
@section('header')
User Management
@endsection
@section('content')

<div class="container p-4" >
  <h2 class="text-center">Detail Pembayaran Kas <br> {{$siswa->nama}} </h2>
  <hr>
  <br>
      <a class="btn btn-md btn-success" href="/owner/siswa/{{$siswa->user_id}}/add" >Tambah Pembayaran</a>
    <br><br>
    <h4>Total Pembayaran : <span class="text-danger">{{$siswa->user_bayar}}</span></h4>
  <table class="table">
    <tr>
      <th>No</th>
      <th>Jumlah bayar</th>
      <th>At</th>
      <th>Opsi</th>
    </tr>
    <?php
      $no = 1;
        // $users = DB::table('users')->where('status', '=', 'user')->get();
    ?>
    @foreach($detail as $mysiswa)
    <tr>
        <td>{{$no++}}</td>
        <td>{{$mysiswa->bayar}}</td>
        <td>{{$mysiswa->created_at}}</td>
        <td>
          <form class="" action="/owner/siswa/{{$mysiswa->id}}/destroy" method="post">
          {{ csrf_field() }}
          <input type="number" name="pemasukan_id" value="{{$mysiswa->pemasukan_id}}" hidden>
            <button type="submit" class="btn btn-group btn-danger" name="button"><i class="fa fa-trash"></i> Hapus</button>
          </form>
        </td>
    </tr>
    @endforeach
  </table>
</div>
@endsection
@section('js')

@endsection
