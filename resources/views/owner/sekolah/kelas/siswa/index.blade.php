
@extends('layouts.lte')
@section('css')
  <style>

    .container{
      background: white;
      border-radius: 4px;
    }
  </style>
@endsection
@section('header')
User Management
@endsection
@section('content')

<div class="container p-4" >
  <h1 class="text-center">Siswa</h1>
  <hr>
  <?php
    $kas = \App\Kas::where('kas.kelas_id', $kelas->id)->first();


   ?>

   <form class="form" action="/owner/kelas/{{$kelas->id}}/kas_mingguan" method="post">
     {{csrf_field()}}
     <h4>
       <button type="submit" class="btn btn-danger" name="button"><i class="fa fa-plus"></i> Update Kas
       </button>
       PEMBAYARAN KAS BULAN INI : {{$kas->kas_bayar}}
     </h4>
   </form>
<br>
  <table class="table">
    <tr>
      <th>No</th>
      <th>NIS</th>
      <th>Nama Siswa</th>
      <th>Status</th>
      <th>Opsi</th>
    </tr>
    <?php
      $no = 1;
        // $users = DB::table('users')->where('status', '=', 'user')->get();
    ?>
    @foreach($siswa as $mysiswa)
    <tr>
        <td>{{$no++}}</td>
        <td>{{$mysiswa->nis}}</td>
        <td>{{$mysiswa->nama}}</td>
        <td>
          @if($mysiswa->user_bayar >= $mysiswa->kas_bayar)
          <span class="text-success"><b> Lunas</b></span>
          @endif
          @if($mysiswa->user_bayar < $mysiswa->kas_bayar)
          <span class="text-danger"><b> Belum Lunas !!</b></span>
          @endif
        </td>
        <td>
          <button type="button" class="btn btn-group btn-primary" name="button" onclick="location.href='/owner/siswa/{{$mysiswa->id}}'"><i class="fa fa-view"></i> Detail</button>
        </td>
    </tr>
    @endforeach
  </table>
</div>
@endsection
@section('js')

@endsection
