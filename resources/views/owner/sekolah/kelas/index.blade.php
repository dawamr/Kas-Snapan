
@extends('layouts.lte')
@section('css')
  <style>

    .container{
      background: white;
      border-radius: 4px;
    }
  </style>
@endsection
@section('header')
User Management
@endsection
@section('content')
<div class="container p-4" id="vue">
  <h1 class="text-center">Kelas</h1>
  <hr>
  <br>
  <div class="col-md-12">
      <div class="col-md-6">
        <div class="btn-group" role="group" aria-label="basic example">
          <form class="" method="post">
            <div class="dropdown">
              <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" name="button">Pilih Tahun Angkatan<span class="caret"></span></button>
              <ul class="dropdown-menu">
                @foreach($tahun as $mytahun)
                <li class="bg-waring">
                  <a href="/owner/angkatan/{{$mytahun->tahun1}}">{{$mytahun->tahun1}} - {{$mytahun->tahun2}}</a>
                </li>
                @endforeach
              </ul>
            </div>
          </form>
          <button type="button" class="btn btn-secondary btn-success" name="button" onclick="location.href='/petugas'"><i class="fa fa-plus">Tambah Kelas</i> &nbsp Beranda</button>
        </div>

      </div>
  </div>
<br><br>
  <table class="table">
    <tr>
      <th>No</th>
      <th>Nama Kelas</th>
      <th>Jurusan</th>
      <th>Opsi</th>
    </tr>
    <?php
      $no = 1;
        // $users = DB::table('users')->where('status', '=', 'user')->get();
    ?>
    @foreach($kelas as $mykelas)
    <tr>
        <td>{{$no++}}</td>
        <td>
          <a href="/owner/kelas/{{$mykelas->id}}">{{$mykelas->nama}}</a>
        </td>
        <td>{{$mykelas->jurusan}}</td>
        <td>
          <button type="button" class="btn btn-sm btn-warning" name="button" onclick="location.href='/owner/kelas/{{$mykelas->id}}/edit'"><i class="fa fa-pencil"></i> Edit</button>
        </td>
    </tr>
    @endforeach
  </table>
  <li>{{$kelas->links()}}</li>
</div>
@endsection
@section('js')
<!-- <script type="text/javascript">
new Vue({
  Vue.component('kelas',{
        template: '#kelas-template',
      });
  data: {
    el: #vue,
      currentView : 'kelas',
    kelas: []
  },
  created:function(){
    this.readKelas()
  },
  methods : {
    readKelas(){
      var vm = this
      axios.get('/owner/api/kelas')
        .then(function (response) {
        })
    },
  }

});
</script> -->
@endsection
