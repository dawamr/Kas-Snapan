@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body text-center">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in! With {{Auth::user()->nama}}
                </div>
                    @if(Auth::user()->hasRole('petugas'))
                      <form class="form text-center" action="index.html" method="post">
                        <div class="row">
                          <div class="col-md-2">

                          </div>
                          <div class="col-md-8 text-center">
                          <input class="form-control text-center" type="password" style="font-family:comic sans ms" name="" value="">
                          </div>
                          <div class="col-md-2">

                          </div>
                        </div>
                        <br>
                        <button type="submit" class="btn btn-md btn-danger text-center" name="button">Activated Code !</button>
                      </form>
                      <br>
                    @endif
            </div>
        </div>
    </div>
</div>
@endsection
